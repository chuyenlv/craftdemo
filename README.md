# README #

Demo craftcms 2.

### Project Requirements
* [Docksal](http://docksal.readthedocs.io/en/master/env-setup/)

### Project Setup
* `git clone git@bitbucket.org:chuyenlv/craftdemo.git craftdemo`
* `cd craftdemo`
* `fin up`
* `fin sqli ./default.sql`
* Point your browser to: `http://craftcms.docksal`