/*jslint browser: true*/
/*global $, jQuery, Modernizr, enquire*/
(function (window, document, $) {
  var $html = $('html'),
    mobileOnly = "screen and (max-width:47.9375em)", // 767px.
    mobileLandscape = "(min-width:30em)", // 480px.
    tablet = "(min-width:48em)"; // 768px.
  // Add  functionality here.

  // Table responsive
  var $table = $('table');
  if ($table.length &&
    !$table.parent().hasClass('table-responsive')) {
    $table.not($table.find('table')).wrap('<div class="table-responsive"></div>');
  }

  // js-play-video
  var $jsPlayVideo = $('.js-play-video'),
      playVideo = function (e) {
    var $iframeVimeo = $(this).find('.vimeo-embed'),
        $iframeYoutube = $(this).find('.youtube-embed');
    $(this).addClass("play-video");
    if ($iframeVimeo.length) {
      var player = Froogaloop($iframeVimeo[0]);
      player.api('play');
    }
    if ($iframeYoutube.length) {
      $iframeYoutube[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
    }
  };
  if ($jsPlayVideo.length) {
    $jsPlayVideo.on('click', playVideo);
  }

  var $jsHeader = $('.js-header'),
      showNavFlag = 'show-nav',
      $menuIconBtn = $('.menu-icon', $jsHeader);
  $menuIconBtn.on('click', function (e) {
    $(this).closest('.js-header').toggleClass(showNavFlag);
  });

  // Header Scroll
  var scrollHeaderFlag = 'scroll-header',
      scrollHeader = function (e) {
        e.preventDefault();
        var $this = $(this),
            scrollTop = $this.scrollTop(),
            heightBeginEffect = 50;
        if (scrollTop > heightBeginEffect && !$jsHeader.hasClass(scrollHeaderFlag)) {
          $jsHeader.addClass(scrollHeaderFlag);
        }
        else if (scrollTop < heightBeginEffect && $jsHeader.hasClass(scrollHeaderFlag)) {
          $jsHeader.removeClass(scrollHeaderFlag);
        }
      };
  $(window).on('scroll', scrollHeader);

}(this, this.document, this.jQuery));
