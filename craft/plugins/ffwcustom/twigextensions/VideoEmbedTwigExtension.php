<?php

namespace Craft;

use Twig_Extension;
use Twig_Filter_Method;

class VideoEmbedTwigExtension extends \Twig_Extension {

  public function getFilters() {
    return array(
      'videoEmbedRender' => new Twig_Filter_Method($this, 'videoEmbedRender'),
    );
  }

  public function videoEmbedRender($input, $options = array()) {
    $data = $this->getIdAndProviderFromInput($input);
    if (empty($data)) {
      return "";
    }

    $strIframe = "";
    switch ($data['provider']) {
      case 'youtube':
        $strIframe = $this->processYoutube($data['vid'], $options);
        break;

      case 'vimeo':
        $strIframe = $this->processVimeo($data['vid'], $options);
        break;
    }

    return $strIframe;
  }

  public static function getIdAndProviderFromInput($input) {
    preg_match('/^https?:\/\/(www\.)?((?!.*list=)youtube\.com\/watch\?.*v=|youtu\.be\/)(?<id>[0-9A-Za-z_-]*)/', $input, $matches);
    if (isset($matches['id'])) {
      return ['vid' => $matches['id'], 'provider' => 'youtube'];
    }

    preg_match('/^https?:\/\/(www\.)?vimeo.com\/(channels\/[a-zA-Z0-9]*\/)?(?<id>[0-9]*)(\/[a-zA-Z0-9]+)?(\#t=(\d+)s)?$/', $input, $matches);
    if (isset($matches['id'])) {
      return ['vid' => $matches['id'], 'provider' => 'vimeo'];
    }

    return [];
  }

  public function processYoutube($vid, $params) {
    $attributes = array(
      'width' => 960,
      'height' => 540,
      'frameborder' => '0',
      'allowfullscreen' => 'allowfullscreen',
      'class' => 'youtube-embed'
    );

    $query = array(
      'enablejsapi' => 1,
      'version' => 3,
    );

    if (isset($params['autoplay']) && $params['autoplay']) {
      $query['autoplay'] = 1;
      unset($params['autoplay']);
    }

    if (isset($params['loop']) && $params['loop']) {
      $query['loop'] = 1;
      unset($params['loop']);
    }

    foreach ($params as $key => $val) {
      $attributes[$key] = $val;
    }

    if ($attributes['allowfullscreen'] == 'allowfullscreen') {
      $attributes['msallowfullscreen'] = 'msallowfullscreen';
      $attributes['oallowfullscreen'] = 'oallowfullscreen';
      $attributes['webkitallowfullscreen'] = 'webkitallowfullscreen';
    }

    $queryStr = http_build_query($query);
    $url = sprintf('https://www.youtube.com/embed/%s?%s', $vid, $queryStr);

    return $this->render($url, $attributes);
  }

  public function processVimeo($vid, $params) {
    $attributes = array(
      'width' => 960,
      'height' => 540,
      'frameborder' => '0',
      'allowfullscreen' => 'allowfullscreen',
      'class' => 'vimeo-embed'
    );

    $query = array();

    if (isset($params['autoplay']) && $params['autoplay']) {
      $query['autoplay'] = 1;
      unset($params['autoplay']);
    }

    if (isset($params['loop']) && $params['loop']) {
      $query['loop'] = 1;
      unset($params['loop']);
    }

    foreach ($params as $key => $val) {
      $attributes[$key] = $val;
    }

    $url = sprintf('https://player.vimeo.com/video/%s', $vid);
    if (!empty($query)) {
      $queryStr = http_build_query($query);
      $url .= '?' . $queryStr;
    }

    return $this->render($url, $attributes);
  }

  public function render($url, $attrs) {
    $attrsHtml = "";
    foreach ($attrs as $key => $val) {
      $attrsHtml .= sprintf(' %s=%s', $key, $val);
    }

    $originalPath = craft()->path->getTemplatesPath();
    $myPath = craft()->path->getPluginsPath() . 'ffwcustom/templates/';
    craft()->path->setTemplatesPath($myPath);

    $markup = craft()->templates->render('_vimeoEmbed.html', array(
      'player_url' => $url,
      'attrs' => $attrsHtml,
    ));

    craft()->path->setTemplatesPath($originalPath);

    return TemplateHelper::getRaw($markup);
  }
}
