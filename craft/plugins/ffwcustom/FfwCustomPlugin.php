<?php

namespace Craft;

class FfwCustomPlugin extends BasePlugin {

  public function addTwigExtension() {
    Craft::import('plugins.ffwcustom.twigextensions.VideoEmbedTwigExtension');
    return new VideoEmbedTwigExtension();
  }

  public function getName() {
    return Craft::t('FFW custom');
  }

  public function getVersion() {
    return '0.0.1';
  }

  function getDeveloper() {
    return 'LC';
  }

  function getDeveloperUrl() {
    return 'https://github.com/chuyenlv';
  }
}
