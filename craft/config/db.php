<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

if (isset($_ENV['PLATFORM_RELATIONSHIPS'])) {
  $relationships = json_decode(base64_decode($_ENV['PLATFORM_RELATIONSHIPS']), true);
  if (isset($relationships['database'][0])) {
    $database = $relationships['database'][0];

    return array(
    	'*' => array(
	      'tablePrefix' => 'cr',
    		'server' => $database['host'],
	      'user' => $database['username'],
	      'password' => $database['password'],
	      'database' => $database['path'],
	      'port' => $database['port'],
    	)
    );
  }
}

return array(
	'*' => array(
		'tablePrefix' => 'cr',
	),
	'craftcms.docksal' => array(
		'server' => 'db',
		'user' => 'user',
		'password' => 'user',
		'database' => 'default',
	)
);
