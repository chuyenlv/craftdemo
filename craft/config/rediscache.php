<?php

if (isset($_ENV['PLATFORM_RELATIONSHIPS'])) {
  $relationships = json_decode(base64_decode($_ENV['PLATFORM_RELATIONSHIPS']), true);

  if (!empty($relationships['redis'][0])) {
  	$redis = $relationships['redis'][0];

  	return array(
			'hostname' => $redis['host'],
			'port' => $redis['port'],
			'password' => '',
			'database' => 0,
			'timeout' => null,
		);
  }
}

return array(
	/**
	 * Hostname to use for connecting to the redis server. Defaults to 'localhost'.
	 */
	'hostname' => 'localhost',

	/**
	 * The port to use for connecting to the redis server. Default port is 6379.
	 */
	'port' => 6379,

	/**
	 * The password to use to authenticate with the redis server. If not set, no AUTH command will be sent.
	 */
	'password' => '',

	/**
	 * The redis database to use. This is an integer value starting from 0. Defaults to 0.
	 */
	'database' => 0,

	/**
	 * Timeout to use for connection to redis. If not set the timeout set in php.ini will be used:
	 * ini_get("default_socket_timeout")
	 */
	'timeout' => null,
);
