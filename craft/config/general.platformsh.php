<?php

// Set the site base URL and base PATH from the Platform.sh routes.
if (isset($_ENV['PLATFORM_ROUTES'])) {
  $routes = json_decode(base64_decode($_ENV['PLATFORM_ROUTES']), TRUE);
  $settings['trusted_host_patterns'] = [];
  foreach ($routes as $url => $route) {
    $host = parse_url($url, PHP_URL_HOST);
    if ($host !== FALSE && $route['type'] == 'upstream') {
      // Note: This won't work for wildcard-based routes. That will require
      // additional inspection of the request itself.
      $settings['*']['environmentVariables']['baseUrl'] = $url;
      $settings['*']['environmentVariables']['basePath'] = '/app/public/';
      $settings['*']['cacheMethod'] = 'redis';
    }
  }
}
