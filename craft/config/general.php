<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

$settings = array(
	'*' => array(
		// Default Week Start Day (0 = Sunday, 1 = Monday...)
		'defaultWeekStartDay' => 0,
		// Enable CSRF Protection
		'enableCsrfProtection' => true,
		// Whether "index.php" should be visible in URLs (true, false, "auto")
		'omitScriptNameInUrls' => true,
		// Control Panel trigger word
		'cpTrigger' => 'admin',
		// Dev Mode (see https://craftcms.com/support/dev-mode)
		'devMode' => ($_SERVER['SERVER_NAME'] == 'craftcms.docksal' ? true : false),
		// Embedded assets
		'extraAllowedFileExtensions' => 'json',
	),
	'craftcms.docksal' => array(
		'environmentVariables' => array(
			'basePath' => '/var/www/public/',
			'baseUrl' => 'http://craftcms.docksal'
		)
	),
);

// Automatic Platform.sh settings.
$platformsh_config = CRAFT_CONFIG_PATH . 'general.platformsh.php';

if (file_exists($platformsh_config)) {
  include $platformsh_config;
}

return $settings;
