-- MySQL dump 10.13  Distrib 5.6.35, for Linux (x86_64)
--
-- Host: localhost    Database: default
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cr_amnav_navs`
--

DROP TABLE IF EXISTS `cr_amnav_navs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_amnav_navs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_amnav_navs_handle_unq_idx` (`handle`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_amnav_navs`
--

LOCK TABLES `cr_amnav_navs` WRITE;
/*!40000 ALTER TABLE `cr_amnav_navs` DISABLE KEYS */;
INSERT INTO `cr_amnav_navs` VALUES (1,'Main','main','{\"entrySources\":[\"singles\",\"section:3\"],\"maxLevels\":\"\",\"canMoveFromLevel\":\"\",\"canDeleteFromLevel\":\"\"}','2017-11-16 04:58:11','2017-11-16 04:58:11','cb7f5f74-b3fe-4d76-af0a-8ababc98b10d');
/*!40000 ALTER TABLE `cr_amnav_navs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_amnav_nodes`
--

DROP TABLE IF EXISTS `cr_amnav_nodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_amnav_nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `navId` int(10) NOT NULL,
  `parentId` int(10) DEFAULT NULL,
  `order` int(10) DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listClass` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blank` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `elementId` int(10) DEFAULT NULL,
  `elementType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_amnav_nodes_elementId_elementType_locale_idx` (`elementId`,`elementType`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_amnav_nodes`
--

LOCK TABLES `cr_amnav_nodes` WRITE;
/*!40000 ALTER TABLE `cr_amnav_nodes` DISABLE KEYS */;
INSERT INTO `cr_amnav_nodes` VALUES (1,1,0,0,'Home',NULL,'',0,1,2,'Entry','en_us','2017-11-16 05:00:37','2017-11-17 09:31:30','f0e8bb25-c35e-4cd9-9999-2117c11e7a65'),(2,1,0,1,'Landing page',NULL,'',0,1,5,'Entry','en_us','2017-11-16 05:00:52','2017-11-16 12:31:27','dd4382a0-7b7f-46a3-82bc-db93d3fb072e'),(3,1,0,2,'News','/news',NULL,0,1,NULL,NULL,'en_us','2017-11-17 09:14:27','2017-11-17 09:14:27','023dc9ca-c034-42da-81c5-254ef65a7cfb');
/*!40000 ALTER TABLE `cr_amnav_nodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_assetfiles`
--

DROP TABLE IF EXISTS `cr_assetfiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_assetfiles` (
  `id` int(11) NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kind` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `size` bigint(20) unsigned DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_assetfiles_filename_folderId_unq_idx` (`filename`,`folderId`),
  KEY `cr_assetfiles_sourceId_fk` (`sourceId`),
  KEY `cr_assetfiles_folderId_fk` (`folderId`),
  CONSTRAINT `cr_assetfiles_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `cr_assetfolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_assetfiles_id_fk` FOREIGN KEY (`id`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_assetfiles_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `cr_assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_assetfiles`
--

LOCK TABLES `cr_assetfiles` WRITE;
/*!40000 ALTER TABLE `cr_assetfiles` DISABLE KEYS */;
INSERT INTO `cr_assetfiles` VALUES (4,1,1,'example-3.jpg','image',940,529,97291,'2017-11-16 02:36:33','2017-11-16 02:36:33','2017-11-16 02:36:33','6077d4ed-c60d-43f1-9d64-e239beb0fc19'),(7,1,1,'example-1.jpg','image',940,529,65962,'2017-11-16 03:32:10','2017-11-16 03:32:10','2017-11-16 03:32:10','d9a22aa6-6366-4073-8f40-4bfa77673aa2'),(8,1,1,'example-2.jpg','image',940,529,131739,'2017-11-16 03:32:10','2017-11-16 03:32:10','2017-11-16 03:32:10','77b94904-cc11-465d-8dc9-c1b5894950c8'),(12,1,1,'embed_Content-Previewing.json','json',NULL,NULL,916,'2017-11-16 03:57:14','2017-11-16 03:57:14','2017-11-16 03:57:14','fa50a4db-925c-4bc9-a74c-c1dcd9aec06d');
/*!40000 ALTER TABLE `cr_assetfiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_assetfolders`
--

DROP TABLE IF EXISTS `cr_assetfolders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_assetfolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_assetfolders_name_parentId_sourceId_unq_idx` (`name`,`parentId`,`sourceId`),
  KEY `cr_assetfolders_parentId_fk` (`parentId`),
  KEY `cr_assetfolders_sourceId_fk` (`sourceId`),
  CONSTRAINT `cr_assetfolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `cr_assetfolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_assetfolders_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `cr_assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_assetfolders`
--

LOCK TABLES `cr_assetfolders` WRITE;
/*!40000 ALTER TABLE `cr_assetfolders` DISABLE KEYS */;
INSERT INTO `cr_assetfolders` VALUES (1,NULL,1,'Media','','2017-11-15 09:05:25','2017-11-15 09:05:25','7eb8ea1d-a89e-40cd-b7f9-8442b04e69bb');
/*!40000 ALTER TABLE `cr_assetfolders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_assetindexdata`
--

DROP TABLE IF EXISTS `cr_assetindexdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_assetindexdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sourceId` int(10) NOT NULL,
  `offset` int(10) NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recordId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_assetindexdata_sessionId_sourceId_offset_unq_idx` (`sessionId`,`sourceId`,`offset`),
  KEY `cr_assetindexdata_sourceId_fk` (`sourceId`),
  CONSTRAINT `cr_assetindexdata_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `cr_assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_assetindexdata`
--

LOCK TABLES `cr_assetindexdata` WRITE;
/*!40000 ALTER TABLE `cr_assetindexdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_assetindexdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_assetsources`
--

DROP TABLE IF EXISTS `cr_assetsources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_assetsources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_assetsources_name_unq_idx` (`name`),
  UNIQUE KEY `cr_assetsources_handle_unq_idx` (`handle`),
  KEY `cr_assetsources_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `cr_assetsources_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `cr_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_assetsources`
--

LOCK TABLES `cr_assetsources` WRITE;
/*!40000 ALTER TABLE `cr_assetsources` DISABLE KEYS */;
INSERT INTO `cr_assetsources` VALUES (1,'Media','media','Local','{\"path\":\"{basePath}uploads\\/\",\"publicURLs\":\"1\",\"url\":\"{baseUrl}\\/uploads\\/\"}',1,137,'2017-11-15 09:05:25','2017-11-17 09:57:58','01df85c2-da34-40fc-86dc-272b7975c2e2');
/*!40000 ALTER TABLE `cr_assetsources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_assettransformindex`
--

DROP TABLE IF EXISTS `cr_assettransformindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_assettransformindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT NULL,
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_assettransformindex_sourceId_fileId_location_idx` (`sourceId`,`fileId`,`location`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_assettransformindex`
--

LOCK TABLES `cr_assettransformindex` WRITE;
/*!40000 ALTER TABLE `cr_assettransformindex` DISABLE KEYS */;
INSERT INTO `cr_assettransformindex` VALUES (1,4,'example-3.jpg',NULL,'_1400x560_crop_center-center_95',1,1,0,'2017-11-17 07:22:09','2017-11-17 07:22:09','2017-11-17 07:22:11','945c61e5-2373-41e4-84a5-5042263674b2'),(2,4,'example-3.jpg',NULL,'_1024x576_crop_center-center_95',1,1,0,'2017-11-17 07:22:09','2017-11-17 07:22:09','2017-11-17 07:22:11','dfb8d78f-f17f-4c32-98c5-e8e70e91c7c2'),(3,4,'example-3.jpg',NULL,'_768x720_crop_center-center_95',1,1,0,'2017-11-17 07:22:09','2017-11-17 07:22:09','2017-11-17 07:22:11','e3229088-1c16-4234-bb48-830cf71bfc8a'),(4,7,'example-1.jpg',NULL,'_960x540_crop_center-center_95',1,1,0,'2017-11-17 07:22:10','2017-11-17 07:22:10','2017-11-17 07:22:11','86d7bf88-7e4b-4413-baa1-e4262b158942'),(5,8,'example-2.jpg',NULL,'_960x540_crop_center-center_95',1,1,0,'2017-11-17 07:22:10','2017-11-17 07:22:10','2017-11-17 07:22:11','441017a5-216c-4492-ba52-94d282130ad3'),(6,4,'example-3.jpg',NULL,'_960x540_crop_center-center_95',1,1,0,'2017-11-17 07:22:10','2017-11-17 07:22:10','2017-11-17 07:22:11','87580cb1-07ba-41b5-b3fe-652d8c63f588'),(7,4,'example-3.jpg',NULL,'_1400x275_crop_center-center_95',1,1,0,'2017-11-20 03:04:10','2017-11-20 03:04:10','2017-11-20 03:04:10','83ea565a-0df7-4c70-a123-59d48e7f3dab'),(8,4,'example-3.jpg',NULL,'_1024x275_crop_center-center_95',1,1,0,'2017-11-20 03:04:10','2017-11-20 03:04:10','2017-11-20 03:04:10','4ec83c49-4bd8-4586-85f0-e9769cacf5d0'),(9,4,'example-3.jpg',NULL,'_768x275_crop_center-center_95',1,1,0,'2017-11-20 03:04:10','2017-11-20 03:04:10','2017-11-20 03:04:10','e3c2e368-dc40-45a2-9b67-54727e20d465');
/*!40000 ALTER TABLE `cr_assettransformindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_assettransforms`
--

DROP TABLE IF EXISTS `cr_assettransforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_assettransforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mode` enum('stretch','fit','crop') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'center-center',
  `height` int(10) DEFAULT NULL,
  `width` int(10) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quality` int(10) DEFAULT NULL,
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_assettransforms_name_unq_idx` (`name`),
  UNIQUE KEY `cr_assettransforms_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_assettransforms`
--

LOCK TABLES `cr_assettransforms` WRITE;
/*!40000 ALTER TABLE `cr_assettransforms` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_assettransforms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_categories`
--

DROP TABLE IF EXISTS `cr_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_categories_groupId_fk` (`groupId`),
  CONSTRAINT `cr_categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `cr_categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_categories_id_fk` FOREIGN KEY (`id`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_categories`
--

LOCK TABLES `cr_categories` WRITE;
/*!40000 ALTER TABLE `cr_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_categorygroups`
--

DROP TABLE IF EXISTS `cr_categorygroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_categorygroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_categorygroups_name_unq_idx` (`name`),
  UNIQUE KEY `cr_categorygroups_handle_unq_idx` (`handle`),
  KEY `cr_categorygroups_structureId_fk` (`structureId`),
  KEY `cr_categorygroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `cr_categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `cr_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `cr_categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `cr_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_categorygroups`
--

LOCK TABLES `cr_categorygroups` WRITE;
/*!40000 ALTER TABLE `cr_categorygroups` DISABLE KEYS */;
INSERT INTO `cr_categorygroups` VALUES (1,12,134,'Category','category',1,'','2017-11-17 07:26:08','2017-11-17 07:26:08','e1ae7a90-7e1f-48ad-8741-37e8886beb19');
/*!40000 ALTER TABLE `cr_categorygroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_categorygroups_i18n`
--

DROP TABLE IF EXISTS `cr_categorygroups_i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_categorygroups_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_categorygroups_i18n_groupId_locale_unq_idx` (`groupId`,`locale`),
  KEY `cr_categorygroups_i18n_locale_fk` (`locale`),
  CONSTRAINT `cr_categorygroups_i18n_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `cr_categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_categorygroups_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `cr_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_categorygroups_i18n`
--

LOCK TABLES `cr_categorygroups_i18n` WRITE;
/*!40000 ALTER TABLE `cr_categorygroups_i18n` DISABLE KEYS */;
INSERT INTO `cr_categorygroups_i18n` VALUES (1,1,'en_us','category/{slug}','{parent.uri}/{slug}','2017-11-17 07:26:08','2017-11-17 07:26:08','26bb4a33-90b3-4dff-91e0-2402aec4a6eb');
/*!40000 ALTER TABLE `cr_categorygroups_i18n` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_content`
--

DROP TABLE IF EXISTS `cr_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_body` text COLLATE utf8_unicode_ci,
  `field_heading` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_description` text COLLATE utf8_unicode_ci,
  `field_position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_linkIt` text COLLATE utf8_unicode_ci,
  `field_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_align` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_content_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `cr_content_title_idx` (`title`),
  KEY `cr_content_locale_fk` (`locale`),
  CONSTRAINT `cr_content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_content_locale_fk` FOREIGN KEY (`locale`) REFERENCES `cr_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_content`
--

LOCK TABLES `cr_content` WRITE;
/*!40000 ALTER TABLE `cr_content` DISABLE KEYS */;
INSERT INTO `cr_content` VALUES (1,1,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-11-15 02:22:09','2017-11-15 02:22:09','b742947b-eb7d-4c00-b33c-aa280de0bec0'),(2,2,'en_us','Welcome to Craft!','<p>It’s true, this site doesn’t have a whole lot of content yet, but don’t worry. Our web developers have just installed the CMS, and they’re setting things up for the content editors this very moment. Soon Craftcms.docksal will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.</p>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-11-15 02:22:10','2017-11-17 09:31:30','c1173ada-8bf8-4c96-ac53-f660bee44515'),(3,3,'en_us','We just installed Craft!','<p>Craft is the CMS that’s powering Craftcms.docksal. It’s beautiful, powerful, flexible, and easy-to-use, and it’s made by Pixel &amp; Tonic. We can’t wait to dive in and see what it’s capable of!</p>\n<!--pagebreak-->\n<p>This is even more captivating content, which you couldn’t see on the News index page because it was entered after a Page Break, and the News index template only likes to show the content on the first page.</p>\n<p>Craft: a nice alternative to Word, if you’re making a website.</p>',NULL,'Craft is the CMS that’s powering Craftcms.docksal. It’s beautiful, powerful, flexible, and easy-to-use, and it’s made by Pixel & Tonic.',NULL,NULL,NULL,NULL,NULL,'2017-11-15 02:22:10','2017-11-20 03:17:56','abe240f6-e8a9-4c4b-881f-7da40f33c3b2'),(4,4,'en_us','Example 3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-11-16 02:36:33','2017-11-16 02:36:33','4c2105db-ffae-452c-abd8-15548a008db1'),(5,5,'en_us','Example page landing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-11-16 02:37:21','2017-11-16 12:31:27','cdc05dce-59d9-4a87-8621-45b69859950f'),(6,6,'en_us',NULL,NULL,'Example page landing','This is example block type','center',NULL,NULL,NULL,'center','2017-11-16 02:37:21','2017-11-16 12:31:27','a9c2646e-2a14-4b34-9a24-81b5fcb8bb47'),(7,7,'en_us','Example 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-11-16 03:32:10','2017-11-16 03:32:10','718b3f88-18ae-4a9f-aad8-7def87b0a70d'),(8,8,'en_us','Example 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-11-16 03:32:10','2017-11-16 03:32:10','06244d41-f99e-4930-ac9f-70112d1baafb'),(12,12,'en_us','Content Previewing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-11-16 03:57:14','2017-11-16 03:57:14','d40a1f2e-a974-4c5b-b351-e6a24e94828e'),(13,13,'en_us',NULL,'<p>Proin eget tortor risus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Nulla porttitor accumsan tincidunt. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. </p>\n<p>Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Pellentesque in ipsum id orci porta dapibus. </p>\n<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Sed porttitor lectus nibh. Nulla porttitor accumsan tincidunt.</p>','Preview while you work.','See how your content will look before it goes live with Live Preview.','left','Craft Previewing',NULL,'#ffffff',NULL,'2017-11-16 04:10:41','2017-11-16 12:31:27','bb086e31-ea7f-4467-8d19-5ffe952d184b'),(14,14,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'#ffffff',NULL,'2017-11-16 10:06:05','2017-11-16 12:31:27','f6a3b80d-8754-46a0-bc58-db4a9d4b7b31'),(15,15,'en_us',NULL,NULL,'Example card 1','Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.',NULL,'CraftCMS','{\"type\":\"entry\",\"custom\":\"\",\"entry\":[\"2\"],\"customText\":\"Read more\"}',NULL,NULL,'2017-11-16 10:06:05','2017-11-16 12:31:27','907a5fcf-7663-4f0d-bf1f-c73892402c6b'),(16,16,'en_us',NULL,NULL,'Example card 2','Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.',NULL,'Template','{\"type\":\"custom\",\"custom\":\"http:\\/\\/google.com\",\"entry\":\"\",\"customText\":\"Read more\"}',NULL,NULL,'2017-11-16 10:06:05','2017-11-16 12:31:27','f9805f14-5fcf-43db-b81c-617a76d6669d'),(17,17,'en_us',NULL,NULL,'Example card 3','Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.',NULL,'Integration','{\"type\":\"custom\",\"custom\":\"http:\\/\\/craftcms.com\",\"entry\":\"\",\"customText\":\"Read more\"}',NULL,NULL,'2017-11-16 10:06:05','2017-11-16 12:31:27','4803acd1-3508-4717-8c90-245a31ddb9c0'),(18,18,'en_us',NULL,NULL,'Welcome to Craft!','',NULL,NULL,NULL,NULL,'center','2017-11-17 09:31:30','2017-11-17 09:31:30','f9a94619-f118-40ec-ac30-c1695c856755'),(19,19,'en_us','News video','<p>Content of example news.</p>',NULL,'The teaser of News',NULL,NULL,NULL,NULL,NULL,'2017-11-20 03:16:59','2017-11-20 03:17:35','25af127b-d56e-4c74-97ef-ea2eebc6b8b4'),(20,20,'en_us','craft',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-11-20 03:17:23','2017-11-20 03:17:23','a0bf61ac-42dc-459e-82cf-de3690e2dbd4'),(21,21,'en_us','demo',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-11-20 03:17:32','2017-11-20 03:17:32','0b41641f-4249-4403-8d48-040aa6f771f5');
/*!40000 ALTER TABLE `cr_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_deprecationerrors`
--

DROP TABLE IF EXISTS `cr_deprecationerrors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_deprecationerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fingerprint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `line` smallint(6) unsigned NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `templateLine` smallint(6) unsigned DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `traces` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_deprecationerrors`
--

LOCK TABLES `cr_deprecationerrors` WRITE;
/*!40000 ALTER TABLE `cr_deprecationerrors` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_deprecationerrors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_elementindexsettings`
--

DROP TABLE IF EXISTS `cr_elementindexsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_elementindexsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_elementindexsettings_type_unq_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_elementindexsettings`
--

LOCK TABLES `cr_elementindexsettings` WRITE;
/*!40000 ALTER TABLE `cr_elementindexsettings` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_elementindexsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_elements`
--

DROP TABLE IF EXISTS `cr_elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_elements_type_idx` (`type`),
  KEY `cr_elements_enabled_idx` (`enabled`),
  KEY `cr_elements_archived_dateCreated_idx` (`archived`,`dateCreated`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_elements`
--

LOCK TABLES `cr_elements` WRITE;
/*!40000 ALTER TABLE `cr_elements` DISABLE KEYS */;
INSERT INTO `cr_elements` VALUES (1,'User',1,0,'2017-11-15 02:22:09','2017-11-15 02:22:09','ae24a6d3-029c-42f3-9b5e-cbd2f76c1da7'),(2,'Entry',1,0,'2017-11-15 02:22:10','2017-11-17 09:31:30','4a6ba90e-fa39-4798-930f-8794252f4e13'),(3,'Entry',1,0,'2017-11-15 02:22:10','2017-11-20 03:17:56','4af31952-c401-4be6-9ef0-ffff9b1502dc'),(4,'Asset',1,0,'2017-11-16 02:36:33','2017-11-16 02:36:33','25c24ea7-d5dd-4356-b6fa-f5e7dd16f8c5'),(5,'Entry',1,0,'2017-11-16 02:37:21','2017-11-16 12:31:27','d29da06e-5a66-43af-8ec8-df4f26672e96'),(6,'Neo_Block',1,0,'2017-11-16 02:37:21','2017-11-16 12:31:27','876c2617-c666-4121-b08f-84dd81c26680'),(7,'Asset',1,0,'2017-11-16 03:32:10','2017-11-16 03:32:10','ff00649c-a996-43c6-9e16-c0e9fe44d7f5'),(8,'Asset',1,0,'2017-11-16 03:32:10','2017-11-16 03:32:10','2405a58a-385a-42d4-8f35-d39b0221244c'),(12,'Asset',1,0,'2017-11-16 03:57:14','2017-11-16 03:57:14','9b3f3863-b5f7-472b-b566-bb582df107c1'),(13,'Neo_Block',1,0,'2017-11-16 04:10:41','2017-11-16 12:31:27','a2352dfd-a3a8-41cc-a8a1-c688e133d5de'),(14,'Neo_Block',1,0,'2017-11-16 10:06:05','2017-11-16 12:31:27','73e4b7b7-a9e8-4eb7-9956-4261c744e3bd'),(15,'Neo_Block',1,0,'2017-11-16 10:06:05','2017-11-16 12:31:27','7137d554-aec3-456b-a81a-bc9e1f55b35c'),(16,'Neo_Block',1,0,'2017-11-16 10:06:05','2017-11-16 12:31:27','82a194bf-d4b7-47e7-a96b-457c1956b593'),(17,'Neo_Block',1,0,'2017-11-16 10:06:05','2017-11-16 12:31:27','c6077048-dc1e-49ef-bc32-9dd6d0100a42'),(18,'Neo_Block',1,0,'2017-11-17 09:31:30','2017-11-17 09:31:30','249a7a33-1713-4d7e-ab54-fa9e55b566a6'),(19,'Entry',1,0,'2017-11-20 03:16:59','2017-11-20 03:17:35','0bb6e33f-94fe-4963-bfad-c8474d8430fb'),(20,'Tag',1,0,'2017-11-20 03:17:23','2017-11-20 03:17:23','d54e7669-f167-467e-8c78-0c22fab5e327'),(21,'Tag',1,0,'2017-11-20 03:17:32','2017-11-20 03:17:32','fa7089b4-df9f-4616-9fdb-0965c918454c');
/*!40000 ALTER TABLE `cr_elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_elements_i18n`
--

DROP TABLE IF EXISTS `cr_elements_i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_elements_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_elements_i18n_elementId_locale_unq_idx` (`elementId`,`locale`),
  UNIQUE KEY `cr_elements_i18n_uri_locale_unq_idx` (`uri`,`locale`),
  KEY `cr_elements_i18n_slug_locale_idx` (`slug`,`locale`),
  KEY `cr_elements_i18n_enabled_idx` (`enabled`),
  KEY `cr_elements_i18n_locale_fk` (`locale`),
  CONSTRAINT `cr_elements_i18n_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_elements_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `cr_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_elements_i18n`
--

LOCK TABLES `cr_elements_i18n` WRITE;
/*!40000 ALTER TABLE `cr_elements_i18n` DISABLE KEYS */;
INSERT INTO `cr_elements_i18n` VALUES (1,1,'en_us','',NULL,1,'2017-11-15 02:22:09','2017-11-15 02:22:09','02bc29c4-0674-4947-9d4b-e2828357113a'),(2,2,'en_us','homepage','__home__',1,'2017-11-15 02:22:10','2017-11-17 09:31:30','88baaed1-2412-47aa-997a-172fcd2a5e48'),(3,3,'en_us','we-just-installed-craft','news/2017/we-just-installed-craft',1,'2017-11-15 02:22:10','2017-11-20 03:17:56','2144a5a5-e999-4c9f-b1bb-d1d9d3938f23'),(4,4,'en_us','example-3',NULL,1,'2017-11-16 02:36:33','2017-11-16 02:36:33','d062bb5f-81d5-41d5-b95e-c757c20734ee'),(5,5,'en_us','example-page-landing','example-page-landing',1,'2017-11-16 02:37:21','2017-11-16 12:31:27','a4dc8e3d-4359-41a8-891b-8cee9f88104d'),(6,6,'en_us','',NULL,1,'2017-11-16 02:37:21','2017-11-16 12:31:27','f850ba05-73eb-4592-b324-5332a169e6f2'),(7,7,'en_us','example-1',NULL,1,'2017-11-16 03:32:10','2017-11-16 03:32:10','99bfc5b5-5cd5-4380-9f3a-dd758c23ff75'),(8,8,'en_us','example-2',NULL,1,'2017-11-16 03:32:10','2017-11-16 03:32:10','879550b9-e5fb-4526-b7c9-6c5a2cfa77d1'),(12,12,'en_us','embed-content-previewing',NULL,1,'2017-11-16 03:57:14','2017-11-16 03:57:14','3354f164-4ef0-4710-9d84-e85e0de176d8'),(13,13,'en_us','',NULL,1,'2017-11-16 04:10:41','2017-11-16 12:31:27','9a76fad1-ca7f-4839-ba25-30d2a1eb18d3'),(14,14,'en_us','',NULL,1,'2017-11-16 10:06:05','2017-11-16 12:31:27','dd1a5dda-4be4-4d60-8567-50bad8bdee9d'),(15,15,'en_us','',NULL,1,'2017-11-16 10:06:05','2017-11-16 12:31:27','ecbff84c-e245-4521-806a-c1e3c4a3d257'),(16,16,'en_us','',NULL,1,'2017-11-16 10:06:05','2017-11-16 12:31:27','54e03969-5000-48e5-bd22-f52ca3ae94d3'),(17,17,'en_us','',NULL,1,'2017-11-16 10:06:06','2017-11-16 12:31:27','b5bf2db5-13a5-4856-9eeb-0a235bcaed0f'),(18,18,'en_us','',NULL,1,'2017-11-17 09:31:30','2017-11-17 09:31:30','05535a3d-bbd1-484b-8611-163d96472cf1'),(19,19,'en_us','news-video','news/2017/news-video',1,'2017-11-20 03:16:59','2017-11-20 03:17:35','213f87d1-ce59-4db4-82fc-a7d58414c1fe'),(20,20,'en_us','craft',NULL,1,'2017-11-20 03:17:23','2017-11-20 03:17:23','beb1cb0b-0360-462d-a828-7a85843a74d3'),(21,21,'en_us','demo',NULL,1,'2017-11-20 03:17:32','2017-11-20 03:17:32','baa31197-dc1d-4a75-9020-acfe6219c0b6');
/*!40000 ALTER TABLE `cr_elements_i18n` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_emailmessages`
--

DROP TABLE IF EXISTS `cr_emailmessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_emailmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` char(150) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_emailmessages_key_locale_unq_idx` (`key`,`locale`),
  KEY `cr_emailmessages_locale_fk` (`locale`),
  CONSTRAINT `cr_emailmessages_locale_fk` FOREIGN KEY (`locale`) REFERENCES `cr_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_emailmessages`
--

LOCK TABLES `cr_emailmessages` WRITE;
/*!40000 ALTER TABLE `cr_emailmessages` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_emailmessages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_entries`
--

DROP TABLE IF EXISTS `cr_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_entries_sectionId_idx` (`sectionId`),
  KEY `cr_entries_typeId_idx` (`typeId`),
  KEY `cr_entries_postDate_idx` (`postDate`),
  KEY `cr_entries_expiryDate_idx` (`expiryDate`),
  KEY `cr_entries_authorId_fk` (`authorId`),
  CONSTRAINT `cr_entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `cr_users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_entries_id_fk` FOREIGN KEY (`id`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `cr_sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `cr_entrytypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_entries`
--

LOCK TABLES `cr_entries` WRITE;
/*!40000 ALTER TABLE `cr_entries` DISABLE KEYS */;
INSERT INTO `cr_entries` VALUES (2,1,NULL,NULL,'2017-11-15 02:22:10',NULL,'2017-11-15 02:22:10','2017-11-17 09:31:30','c3484e06-a2a2-4769-8f6a-a084a1458493'),(3,2,2,1,'2017-11-15 02:22:00',NULL,'2017-11-15 02:22:10','2017-11-20 03:17:56','144be92c-3822-426f-b8a1-daa8a0dee08f'),(5,3,3,1,'2017-11-16 02:47:00',NULL,'2017-11-16 02:37:21','2017-11-16 12:31:27','992eed1f-59c9-462a-b63f-6f6052347e11'),(19,2,2,1,'2017-11-20 03:16:00',NULL,'2017-11-20 03:16:59','2017-11-20 03:17:35','427c5dbe-7414-431b-b114-c0039b2c187e');
/*!40000 ALTER TABLE `cr_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_entrydrafts`
--

DROP TABLE IF EXISTS `cr_entrydrafts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_entrydrafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_entrydrafts_entryId_locale_idx` (`entryId`,`locale`),
  KEY `cr_entrydrafts_sectionId_fk` (`sectionId`),
  KEY `cr_entrydrafts_creatorId_fk` (`creatorId`),
  KEY `cr_entrydrafts_locale_fk` (`locale`),
  CONSTRAINT `cr_entrydrafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `cr_users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_entrydrafts_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `cr_entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_entrydrafts_locale_fk` FOREIGN KEY (`locale`) REFERENCES `cr_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cr_entrydrafts_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `cr_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_entrydrafts`
--

LOCK TABLES `cr_entrydrafts` WRITE;
/*!40000 ALTER TABLE `cr_entrydrafts` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_entrydrafts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_entrytypes`
--

DROP TABLE IF EXISTS `cr_entrytypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_entrytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasTitleField` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Title',
  `titleFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_entrytypes_name_sectionId_unq_idx` (`name`,`sectionId`),
  UNIQUE KEY `cr_entrytypes_handle_sectionId_unq_idx` (`handle`,`sectionId`),
  KEY `cr_entrytypes_sectionId_fk` (`sectionId`),
  KEY `cr_entrytypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `cr_entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `cr_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `cr_entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `cr_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_entrytypes`
--

LOCK TABLES `cr_entrytypes` WRITE;
/*!40000 ALTER TABLE `cr_entrytypes` DISABLE KEYS */;
INSERT INTO `cr_entrytypes` VALUES (1,1,136,'Homepage','homepage',1,'Title',NULL,1,'2017-11-15 02:22:10','2017-11-17 09:30:41','91bd37ed-2b7b-4af1-8389-1853856cfc73'),(2,2,135,'News','news',1,'Title',NULL,1,'2017-11-15 02:22:10','2017-11-17 07:28:37','a4ce82bf-a7fe-4f0c-9944-8ccd2533dc96'),(3,3,117,'Landing','landing',1,'Title',NULL,1,'2017-11-15 02:55:18','2017-11-16 08:11:42','fedef354-5f7c-4cb7-88be-821b0bb691b6');
/*!40000 ALTER TABLE `cr_entrytypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_entryversions`
--

DROP TABLE IF EXISTS `cr_entryversions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_entryversions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `num` smallint(6) unsigned NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_entryversions_entryId_locale_idx` (`entryId`,`locale`),
  KEY `cr_entryversions_sectionId_fk` (`sectionId`),
  KEY `cr_entryversions_creatorId_fk` (`creatorId`),
  KEY `cr_entryversions_locale_fk` (`locale`),
  CONSTRAINT `cr_entryversions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `cr_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `cr_entryversions_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `cr_entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_entryversions_locale_fk` FOREIGN KEY (`locale`) REFERENCES `cr_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cr_entryversions_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `cr_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_entryversions`
--

LOCK TABLES `cr_entryversions` WRITE;
/*!40000 ALTER TABLE `cr_entryversions` DISABLE KEYS */;
INSERT INTO `cr_entryversions` VALUES (1,2,1,1,'en_us',1,NULL,'{\"typeId\":\"1\",\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1510712530,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2017-11-15 02:22:10','2017-11-15 02:22:10','2a32225c-effd-4182-bc1f-fa3c9d9ca956'),(2,2,1,1,'en_us',2,NULL,'{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Craftcms.docksal!\",\"slug\":\"homepage\",\"postDate\":1510712530,\"expiryDate\":null,\"enabled\":\"1\",\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Craftcms.docksal will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\"}}','2017-11-15 02:22:10','2017-11-15 02:22:10','1b24701d-505f-477a-a126-1d0ae20417c3'),(3,3,2,1,'en_us',1,NULL,'{\"typeId\":\"2\",\"authorId\":\"1\",\"title\":\"We just installed Craft!\",\"slug\":\"we-just-installed-craft\",\"postDate\":1510712530,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2017-11-15 02:22:10','2017-11-15 02:22:10','56231100-f4b6-40dc-a112-6b521a388d09'),(4,2,1,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Craft!\",\"slug\":\"homepage\",\"postDate\":1510712530,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Craftcms.docksal will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\"}}','2017-11-15 10:36:53','2017-11-15 10:36:53','a7ca6023-c0f4-443b-9977-d44c2df6b70c'),(5,5,3,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Example page landing\",\"slug\":\"example-page-landing\",\"postDate\":null,\"expiryDate\":null,\"enabled\":0,\"parentId\":\"\",\"fields\":{\"14\":\"\",\"12\":{\"new0\":{\"modified\":\"1\",\"type\":\"hero\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"image\":[\"4\"],\"heading\":\"Example page landing\",\"description\":\"This is example block type\"}}}}}','2017-11-16 02:37:21','2017-11-16 02:37:21','2a01ba18-b15c-4b55-8255-6ee48ffc07de'),(6,5,3,1,'en_us',2,'','{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Example page landing\",\"slug\":\"example-page-landing\",\"postDate\":1510800441,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"14\":\"\",\"12\":{\"6\":{\"modified\":\"1\",\"type\":\"hero\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"image\":[\"4\"],\"heading\":\"Example page landing\",\"description\":\"This is example block type\"}}}}}','2017-11-16 02:47:21','2017-11-16 02:47:21','0a966c13-2afb-4ac2-81bf-2b6c7abcbddc'),(7,5,3,1,'en_us',3,'','{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Example page landing\",\"slug\":\"example-page-landing\",\"postDate\":1510800420,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"14\":\"\",\"12\":{\"6\":{\"modified\":\"1\",\"type\":\"hero\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"image\":\"\",\"heading\":\"Example page landing\",\"description\":\"This is example block type\"}}}}}','2017-11-16 03:07:53','2017-11-16 03:07:53','00f2f045-f025-4cd1-b0ba-ee0a981be677'),(8,5,3,1,'en_us',4,'','{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Example page landing\",\"slug\":\"example-page-landing\",\"postDate\":1510800420,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"14\":\"\",\"12\":{\"6\":{\"modified\":\"1\",\"type\":\"hero\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"image\":[\"4\"],\"heading\":\"Example page landing\",\"description\":\"This is example block type\"}}}}}','2017-11-16 03:10:22','2017-11-16 03:10:22','5e443961-2531-49ce-847b-42c26a8a63ee'),(9,5,3,1,'en_us',5,'','{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Example page landing\",\"slug\":\"example-page-landing\",\"postDate\":1510800420,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"14\":\"\",\"12\":{\"6\":{\"modified\":\"1\",\"type\":\"hero\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"image\":[\"4\"],\"heading\":\"Example page landing\",\"description\":\"This is example block type\"}},\"new0\":{\"modified\":\"1\",\"type\":\"slider\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\"},\"new1\":{\"modified\":\"1\",\"type\":\"slideItem\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"7\"],\"heading\":\"Slide 1\",\"description\":\"\"}},\"new2\":{\"modified\":\"1\",\"type\":\"slideItem\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"8\"],\"heading\":\"Slide 2\",\"description\":\"\"}}}}}','2017-11-16 03:32:54','2017-11-16 03:32:54','0162bda0-eaa0-46d3-a420-b1cb7951faec'),(10,5,3,1,'en_us',6,'','{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Example page landing\",\"slug\":\"example-page-landing\",\"postDate\":1510800420,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"14\":\"\",\"12\":{\"6\":{\"modified\":\"1\",\"type\":\"hero\",\"enabled\":\"1\",\"collapsed\":\"1\",\"level\":\"0\",\"fields\":{\"image\":[\"4\"],\"heading\":\"Example page landing\",\"description\":\"This is example block type\"}},\"9\":{\"modified\":\"1\",\"type\":\"slider\",\"enabled\":\"1\",\"collapsed\":\"1\",\"level\":\"0\"},\"10\":{\"modified\":\"1\",\"type\":\"slideItem\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"7\"],\"heading\":\"Slide 1\",\"description\":\"\"}},\"11\":{\"modified\":\"1\",\"type\":\"slideItem\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"8\"],\"heading\":\"Slide 2\",\"description\":\"\"}},\"new0\":{\"modified\":\"1\",\"type\":\"boxVideo\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"videoEmbed\":[\"12\"],\"label\":\"Craft Previewing\",\"description\":\"See how your content will look before it goes live with Live Preview.\",\"position\":\"left\",\"heading\":\"Preview while you work.\",\"body\":\"<p>Proin eget tortor risus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Nulla porttitor accumsan tincidunt. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.&nbsp;<\\/p>\\r\\n<p>Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Pellentesque in ipsum id orci porta dapibus.&nbsp;<\\/p>\\r\\n<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Sed porttitor lectus nibh. Nulla porttitor accumsan tincidunt.<\\/p>\"}}}}}','2017-11-16 04:10:42','2017-11-16 04:10:42','3f2e723e-18c0-414c-935e-a5a72dd1a645'),(11,5,3,1,'en_us',7,'','{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Example page landing\",\"slug\":\"example-page-landing\",\"postDate\":1510800420,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"14\":\"\",\"12\":{\"6\":{\"modified\":\"1\",\"type\":\"hero\",\"enabled\":\"1\",\"collapsed\":\"1\",\"level\":\"0\",\"fields\":{\"image\":[\"4\"],\"heading\":\"Example page landing\",\"description\":\"This is example block type\"}},\"13\":{\"modified\":\"1\",\"type\":\"boxVideo\",\"enabled\":\"1\",\"collapsed\":\"1\",\"level\":\"0\",\"fields\":{\"videoEmbed\":[\"12\"],\"label\":\"Craft Previewing\",\"description\":\"See how your content will look before it goes live with Live Preview.\",\"position\":\"left\",\"heading\":\"Preview while you work.\",\"body\":\"<p>Proin eget tortor risus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Nulla porttitor accumsan tincidunt. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.&nbsp;<\\/p>\\r\\n<p>Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Pellentesque in ipsum id orci porta dapibus.&nbsp;<\\/p>\\r\\n<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Sed porttitor lectus nibh. Nulla porttitor accumsan tincidunt.<\\/p>\"}}}}}','2017-11-16 07:50:46','2017-11-16 07:50:46','cf6eaffc-7933-4b87-b35e-122845bbef2d'),(12,5,3,1,'en_us',8,'','{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Example page landing\",\"slug\":\"example-page-landing\",\"postDate\":1510800420,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"12\":{\"6\":{\"modified\":\"1\",\"type\":\"hero\",\"enabled\":\"1\",\"collapsed\":\"1\",\"level\":\"0\",\"fields\":{\"image\":[\"4\"],\"heading\":\"Example page landing\",\"description\":\"This is example block type\"}},\"13\":{\"modified\":\"1\",\"type\":\"boxVideo\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"color\":\"#ffffff\",\"videoEmbed\":[\"12\"],\"label\":\"Craft Previewing\",\"description\":\"See how your content will look before it goes live with Live Preview.\",\"position\":\"left\",\"heading\":\"Preview while you work.\",\"body\":\"<p>Proin eget tortor risus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Nulla porttitor accumsan tincidunt. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.&nbsp;<\\/p>\\r\\n<p>Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Pellentesque in ipsum id orci porta dapibus.&nbsp;<\\/p>\\r\\n<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Sed porttitor lectus nibh. Nulla porttitor accumsan tincidunt.<\\/p>\"}},\"new0\":{\"modified\":\"1\",\"type\":\"cards\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\"},\"new1\":{\"modified\":\"1\",\"type\":\"cardItem\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"7\"],\"label\":\"CraftCMS\",\"heading\":\"Example card 1\",\"description\":\"Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.\",\"linkIt\":{\"type\":\"entry\",\"custom\":\"\",\"entry\":[\"2\"],\"customText\":\"Read more\"}}},\"new2\":{\"modified\":\"1\",\"type\":\"cardItem\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"8\"],\"label\":\"Template\",\"heading\":\"Example card 2\",\"description\":\"Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.\",\"linkIt\":{\"type\":\"custom\",\"custom\":\"http:\\/\\/google.com\",\"entry\":\"\",\"customText\":\"Read more\"}}},\"new3\":{\"modified\":\"1\",\"type\":\"cardItem\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"4\"],\"label\":\"Integration\",\"heading\":\"Example card 3\",\"description\":\"Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.\",\"linkIt\":{\"type\":\"custom\",\"custom\":\"http:\\/\\/craftcms.com\",\"entry\":\"\",\"customText\":\"Read more\"}}}}}}','2017-11-16 10:06:06','2017-11-16 10:06:06','f661ef4e-d864-4346-a369-b9b55f7758b8'),(13,5,3,1,'en_us',9,'','{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Example page landing\",\"slug\":\"example-page-landing\",\"postDate\":1510800420,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"12\":{\"6\":{\"modified\":\"1\",\"type\":\"hero\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"position\":\"center\",\"image\":[\"4\"],\"heading\":\"Example page landing\",\"description\":\"This is example block type\"}},\"13\":{\"modified\":\"1\",\"type\":\"boxVideo\",\"enabled\":\"1\",\"collapsed\":\"1\",\"level\":\"0\",\"fields\":{\"color\":\"#ffffff\",\"videoEmbed\":[\"12\"],\"label\":\"Craft Previewing\",\"description\":\"See how your content will look before it goes live with Live Preview.\",\"position\":\"left\",\"heading\":\"Preview while you work.\",\"body\":\"<p>Proin eget tortor risus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Nulla porttitor accumsan tincidunt. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.&nbsp;<\\/p>\\r\\n<p>Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Pellentesque in ipsum id orci porta dapibus.&nbsp;<\\/p>\\r\\n<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Sed porttitor lectus nibh. Nulla porttitor accumsan tincidunt.<\\/p>\"}},\"14\":{\"modified\":\"1\",\"type\":\"cards\",\"enabled\":\"1\",\"collapsed\":\"1\",\"level\":\"0\",\"fields\":{\"color\":\"#ffffff\"}},\"15\":{\"modified\":\"1\",\"type\":\"cardItem\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"7\"],\"label\":\"CraftCMS\",\"heading\":\"Example card 1\",\"description\":\"Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.\",\"linkIt\":{\"type\":\"entry\",\"custom\":\"\",\"entry\":[\"2\"],\"customText\":\"Read more\"}}},\"16\":{\"modified\":\"1\",\"type\":\"cardItem\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"8\"],\"label\":\"Template\",\"heading\":\"Example card 2\",\"description\":\"Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.\",\"linkIt\":{\"type\":\"custom\",\"custom\":\"http:\\/\\/google.com\",\"entry\":\"\",\"customText\":\"Read more\"}}},\"17\":{\"modified\":\"1\",\"type\":\"cardItem\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"4\"],\"label\":\"Integration\",\"heading\":\"Example card 3\",\"description\":\"Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.\",\"linkIt\":{\"type\":\"custom\",\"custom\":\"http:\\/\\/craftcms.com\",\"entry\":\"\",\"customText\":\"Read more\"}}}}}}','2017-11-16 10:48:36','2017-11-16 10:48:36','6fbb3631-daa8-4a05-83b7-71800329f2cd'),(14,5,3,1,'en_us',10,'','{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Example page landing\",\"slug\":\"example-page-landing\",\"postDate\":1510800420,\"expiryDate\":null,\"enabled\":1,\"parentId\":\"\",\"fields\":{\"12\":{\"6\":{\"modified\":\"1\",\"type\":\"hero\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"align\":\"center\",\"image\":[\"4\"],\"heading\":\"Example page landing\",\"description\":\"This is example block type\"}},\"13\":{\"modified\":\"1\",\"type\":\"boxVideo\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"color\":\"#ffffff\",\"videoEmbed\":[\"12\"],\"label\":\"Craft Previewing\",\"description\":\"See how your content will look before it goes live with Live Preview.\",\"position\":\"left\",\"heading\":\"Preview while you work.\",\"body\":\"<p>Proin eget tortor risus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Nulla porttitor accumsan tincidunt. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.&nbsp;<\\/p>\\r\\n<p>Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Pellentesque in ipsum id orci porta dapibus.&nbsp;<\\/p>\\r\\n<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Sed porttitor lectus nibh. Nulla porttitor accumsan tincidunt.<\\/p>\"}},\"14\":{\"modified\":\"1\",\"type\":\"cards\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"color\":\"#ffffff\"}},\"15\":{\"modified\":\"1\",\"type\":\"cardItem\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"7\"],\"label\":\"CraftCMS\",\"heading\":\"Example card 1\",\"description\":\"Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.\",\"linkIt\":{\"type\":\"entry\",\"custom\":\"\",\"entry\":[\"2\"],\"customText\":\"Read more\"}}},\"16\":{\"modified\":\"1\",\"type\":\"cardItem\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"8\"],\"label\":\"Template\",\"heading\":\"Example card 2\",\"description\":\"Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.\",\"linkIt\":{\"type\":\"custom\",\"custom\":\"http:\\/\\/google.com\",\"entry\":\"\",\"customText\":\"Read more\"}}},\"17\":{\"modified\":\"1\",\"type\":\"cardItem\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"4\"],\"label\":\"Integration\",\"heading\":\"Example card 3\",\"description\":\"Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.\",\"linkIt\":{\"type\":\"custom\",\"custom\":\"http:\\/\\/craftcms.com\",\"entry\":\"\",\"customText\":\"Read more\"}}}}}}','2017-11-16 12:31:27','2017-11-16 12:31:27','59058698-724d-42c3-810b-b7443d174267'),(15,3,2,1,'en_us',2,'','{\"typeId\":\"2\",\"authorId\":\"1\",\"title\":\"We just installed Craft!\",\"slug\":\"we-just-installed-craft\",\"postDate\":1510712520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Craft is the CMS that\\u2019s powering Craftcms.docksal. It\\u2019s beautiful, powerful, flexible, and easy-to-use, and it\\u2019s made by Pixel & Tonic. We can\\u2019t wait to dive in and see what it\\u2019s capable of!<\\/p>\\r\\n<hr class=\\\"redactor_pagebreak\\\" style=\\\"display:none\\\" unselectable=\\\"on\\\" contenteditable=\\\"false\\\">\\r\\n<p>This is even more captivating content, which you couldn\\u2019t see on the News index page because it was entered after a Page Break, and the News index template only likes to show the content on the first page.<\\/p>\\r\\n<p>Craft: a nice alternative to Word, if you\\u2019re making a website.<\\/p>\",\"25\":\"\",\"16\":\"Craft is the CMS that\\u2019s powering Craftcms.docksal. It\\u2019s beautiful, powerful, flexible, and easy-to-use, and it\\u2019s made by Pixel & Tonic.\",\"18\":[\"12\"],\"2\":\"\"}}','2017-11-17 08:14:41','2017-11-17 08:14:41','6671124e-17e3-4c78-a71f-e034091d6488'),(16,2,1,1,'en_us',4,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Craft!\",\"slug\":\"homepage\",\"postDate\":1510712530,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"12\":{\"new0\":{\"modified\":\"1\",\"type\":\"hero\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"align\":\"center\",\"image\":\"\",\"heading\":\"Welcome to Craft!\",\"description\":\"\"}}}}}','2017-11-17 09:31:30','2017-11-17 09:31:30','806bd98a-2dc9-4e92-ba16-d8a86b2366d3'),(17,3,2,1,'en_us',3,'','{\"typeId\":\"2\",\"authorId\":\"1\",\"title\":\"We just installed Craft!\",\"slug\":\"we-just-installed-craft\",\"postDate\":1510712520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Craft is the CMS that\\u2019s powering Craftcms.docksal. It\\u2019s beautiful, powerful, flexible, and easy-to-use, and it\\u2019s made by Pixel & Tonic. We can\\u2019t wait to dive in and see what it\\u2019s capable of!<\\/p>\\r\\n<hr class=\\\"redactor_pagebreak\\\" style=\\\"display:none\\\" unselectable=\\\"on\\\" contenteditable=\\\"false\\\">\\r\\n<p>This is even more captivating content, which you couldn\\u2019t see on the News index page because it was entered after a Page Break, and the News index template only likes to show the content on the first page.<\\/p>\\r\\n<p>Craft: a nice alternative to Word, if you\\u2019re making a website.<\\/p>\",\"25\":\"\",\"16\":\"Craft is the CMS that\\u2019s powering Craftcms.docksal. It\\u2019s beautiful, powerful, flexible, and easy-to-use, and it\\u2019s made by Pixel & Tonic.\",\"18\":[\"12\"],\"2\":\"\"}}','2017-11-20 02:43:22','2017-11-20 02:43:22','091f27f1-f8b1-4bb2-880e-ae894983c255'),(18,3,2,1,'en_us',4,'','{\"typeId\":\"2\",\"authorId\":\"1\",\"title\":\"We just installed Craft!\",\"slug\":\"we-just-installed-craft\",\"postDate\":1510712520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Craft is the CMS that\\u2019s powering Craftcms.docksal. It\\u2019s beautiful, powerful, flexible, and easy-to-use, and it\\u2019s made by Pixel & Tonic. We can\\u2019t wait to dive in and see what it\\u2019s capable of!<\\/p>\\r\\n<hr class=\\\"redactor_pagebreak\\\" style=\\\"display:none\\\" unselectable=\\\"on\\\" contenteditable=\\\"false\\\">\\r\\n<p>This is even more captivating content, which you couldn\\u2019t see on the News index page because it was entered after a Page Break, and the News index template only likes to show the content on the first page.<\\/p>\\r\\n<p>Craft: a nice alternative to Word, if you\\u2019re making a website.<\\/p>\",\"25\":\"\",\"16\":\"Craft is the CMS that\\u2019s powering Craftcms.docksal. It\\u2019s beautiful, powerful, flexible, and easy-to-use, and it\\u2019s made by Pixel & Tonic.\",\"18\":[\"4\"],\"2\":\"\"}}','2017-11-20 03:04:05','2017-11-20 03:04:05','46e70f84-19ff-4f5c-80e9-431704175782'),(19,19,2,1,'en_us',1,'','{\"typeId\":null,\"authorId\":\"1\",\"title\":\"News video\",\"slug\":\"news-video\",\"postDate\":1511147819,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Content of example news.<\\/p>\",\"25\":\"\",\"16\":\"The teaser of News\",\"18\":[\"12\"],\"2\":\"\"}}','2017-11-20 03:16:59','2017-11-20 03:16:59','575fc349-8302-42f1-ae3f-75d18fe09b80'),(20,19,2,1,'en_us',2,'','{\"typeId\":\"2\",\"authorId\":\"1\",\"title\":\"News video\",\"slug\":\"news-video\",\"postDate\":1511147760,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Content of example news.<\\/p>\",\"25\":\"\",\"16\":\"The teaser of News\",\"18\":[\"12\"],\"2\":[\"20\",\"21\"]}}','2017-11-20 03:17:35','2017-11-20 03:17:35','8d13de4e-d98a-4c1a-8395-a4239acd882e'),(21,3,2,1,'en_us',5,'','{\"typeId\":\"2\",\"authorId\":\"1\",\"title\":\"We just installed Craft!\",\"slug\":\"we-just-installed-craft\",\"postDate\":1510712520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>Craft is the CMS that\\u2019s powering Craftcms.docksal. It\\u2019s beautiful, powerful, flexible, and easy-to-use, and it\\u2019s made by Pixel & Tonic. We can\\u2019t wait to dive in and see what it\\u2019s capable of!<\\/p>\\r\\n<hr class=\\\"redactor_pagebreak\\\" style=\\\"display:none\\\" unselectable=\\\"on\\\" contenteditable=\\\"false\\\">\\r\\n<p>This is even more captivating content, which you couldn\\u2019t see on the News index page because it was entered after a Page Break, and the News index template only likes to show the content on the first page.<\\/p>\\r\\n<p>Craft: a nice alternative to Word, if you\\u2019re making a website.<\\/p>\",\"25\":\"\",\"16\":\"Craft is the CMS that\\u2019s powering Craftcms.docksal. It\\u2019s beautiful, powerful, flexible, and easy-to-use, and it\\u2019s made by Pixel & Tonic.\",\"18\":[\"4\"],\"2\":[\"20\"]}}','2017-11-20 03:17:56','2017-11-20 03:17:56','af2e9a86-ebd3-4550-9557-5f7936a8e8f3');
/*!40000 ALTER TABLE `cr_entryversions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_fieldgroups`
--

DROP TABLE IF EXISTS `cr_fieldgroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_fieldgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_fieldgroups_name_unq_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_fieldgroups`
--

LOCK TABLES `cr_fieldgroups` WRITE;
/*!40000 ALTER TABLE `cr_fieldgroups` DISABLE KEYS */;
INSERT INTO `cr_fieldgroups` VALUES (1,'Common','2017-11-15 02:22:10','2017-11-16 04:01:24','0e0c5739-8664-4641-aab6-332288d3462d'),(2,'Complex','2017-11-15 08:49:41','2017-11-15 08:49:41','6c598b37-dc7e-4abf-9f5b-4beb002d6600');
/*!40000 ALTER TABLE `cr_fieldgroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_fieldlayoutfields`
--

DROP TABLE IF EXISTS `cr_fieldlayoutfields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_fieldlayoutfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  KEY `cr_fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  KEY `cr_fieldlayoutfields_tabId_fk` (`tabId`),
  KEY `cr_fieldlayoutfields_fieldId_fk` (`fieldId`),
  CONSTRAINT `cr_fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `cr_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `cr_fieldlayouts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `cr_fieldlayouttabs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=273 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_fieldlayoutfields`
--

LOCK TABLES `cr_fieldlayoutfields` WRITE;
/*!40000 ALTER TABLE `cr_fieldlayoutfields` DISABLE KEYS */;
INSERT INTO `cr_fieldlayoutfields` VALUES (183,117,79,12,0,1,'2017-11-16 08:11:42','2017-11-16 08:11:42','0dd11fda-2ca3-4c1c-9b2f-8cc679cb7453'),(249,135,95,18,0,1,'2017-11-17 07:28:37','2017-11-17 07:28:37','10426906-2d1b-4aa4-87c6-c59089eb8f96'),(250,135,95,16,1,2,'2017-11-17 07:28:37','2017-11-17 07:28:37','1cdc8ecf-714c-467b-985f-388c51651552'),(251,135,95,25,0,3,'2017-11-17 07:28:37','2017-11-17 07:28:37','f0d7f711-7d46-4fd3-b560-0b6193fc3716'),(252,135,95,1,1,4,'2017-11-17 07:28:37','2017-11-17 07:28:37','0d2c5f94-29dc-4036-b0b4-87f115445528'),(253,135,95,2,0,5,'2017-11-17 07:28:37','2017-11-17 07:28:37','494b6157-a148-49c4-91f1-797a35c011d9'),(254,136,96,12,0,1,'2017-11-17 09:30:41','2017-11-17 09:30:41','d4702de0-240d-4fcb-8917-5428b2fdf5f9'),(255,138,97,24,0,1,'2017-11-20 04:00:41','2017-11-20 04:00:41','f09c1e2e-80e7-481a-b399-d87fe3cc83a0'),(256,138,97,14,0,2,'2017-11-20 04:00:41','2017-11-20 04:00:41','35ea2d3e-ff65-447f-a80c-037c1a33eccd'),(257,138,97,15,1,3,'2017-11-20 04:00:41','2017-11-20 04:00:41','10547186-2ee4-499e-abdd-69586d400a61'),(258,138,97,16,0,4,'2017-11-20 04:00:41','2017-11-20 04:00:41','b8d9064a-22ff-47bb-a020-7019ec3a06d8'),(259,139,98,23,0,1,'2017-11-20 04:00:41','2017-11-20 04:00:41','ed7a43fc-558c-47dd-8a60-940d43d19556'),(260,139,98,15,0,2,'2017-11-20 04:00:41','2017-11-20 04:00:41','ee44b798-3c12-40c6-85cc-e83e4c080334'),(261,140,99,14,1,1,'2017-11-20 04:00:41','2017-11-20 04:00:41','e3dcb6c0-3c6a-4647-ad6c-43a892086fa2'),(262,140,99,21,0,2,'2017-11-20 04:00:41','2017-11-20 04:00:41','fa8e034e-713c-4bd9-a5e4-513e48c95e9f'),(263,140,99,15,1,3,'2017-11-20 04:00:41','2017-11-20 04:00:41','71808e2f-b37c-44a1-80cc-710a29b2ca2a'),(264,140,99,16,0,4,'2017-11-20 04:00:41','2017-11-20 04:00:41','d3056800-f984-454a-9d97-ba7c82da59e6'),(265,140,99,22,0,5,'2017-11-20 04:00:41','2017-11-20 04:00:41','4a84e5da-f74e-4d5f-9191-18675e287546'),(266,141,100,23,0,1,'2017-11-20 04:00:41','2017-11-20 04:00:41','a42fe301-9629-421e-ae7c-b7becc7ce951'),(267,141,100,17,1,2,'2017-11-20 04:00:41','2017-11-20 04:00:41','a1f0f68c-2e7e-411e-87e2-6d700a898f89'),(268,141,100,21,0,3,'2017-11-20 04:00:41','2017-11-20 04:00:41','889489db-2580-496f-8f8b-f0386fab112d'),(269,141,100,16,0,4,'2017-11-20 04:00:41','2017-11-20 04:00:41','dd7e3b00-e390-4981-afae-d6a41e54fc66'),(270,141,100,20,0,5,'2017-11-20 04:00:41','2017-11-20 04:00:41','55914504-76a6-448e-9856-779bb07f2ee2'),(271,141,100,15,0,6,'2017-11-20 04:00:41','2017-11-20 04:00:41','c0aab1fa-aa1f-4b6c-bd4e-a9ab3c361218'),(272,141,100,1,0,7,'2017-11-20 04:00:41','2017-11-20 04:00:41','ef663046-462e-4498-98f9-3fdacdca9e8c');
/*!40000 ALTER TABLE `cr_fieldlayoutfields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_fieldlayouts`
--

DROP TABLE IF EXISTS `cr_fieldlayouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_fieldlayouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_fieldlayouts_type_idx` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_fieldlayouts`
--

LOCK TABLES `cr_fieldlayouts` WRITE;
/*!40000 ALTER TABLE `cr_fieldlayouts` DISABLE KEYS */;
INSERT INTO `cr_fieldlayouts` VALUES (1,'Tag','2017-11-15 02:22:10','2017-11-15 02:22:10','aa540b3b-d236-400f-b56b-e246a614d37d'),(49,'Commerce_Order','2017-11-15 09:28:06','2017-11-15 09:28:06','0035cd8e-48d5-477a-97d0-741baa834048'),(50,'Commerce_Product','2017-11-15 09:28:06','2017-11-15 09:28:06','fef4c2a2-ba60-482e-8c5a-856a72194ba2'),(51,'Commerce_Variant','2017-11-15 09:28:06','2017-11-15 09:28:06','3ba466cd-1be9-4d2d-800c-3ce54e957648'),(52,'Commerce_Product','2017-11-15 09:28:06','2017-11-15 09:28:06','d7af1e0f-ab7b-4601-a079-54488c001665'),(53,'Commerce_Variant','2017-11-15 09:28:06','2017-11-15 09:28:06','bf0e0518-640c-4e16-addf-f96fe3879925'),(117,'Entry','2017-11-16 08:11:42','2017-11-16 08:11:42','e6df9625-0653-4018-b732-2cd2ef0d4e35'),(134,'Category','2017-11-17 07:26:08','2017-11-17 07:26:08','97c4156d-53d3-422a-94e9-f5bb7a88d9a4'),(135,'Entry','2017-11-17 07:28:37','2017-11-17 07:28:37','c9bdabcc-3ab6-468b-8211-6dd1a583aea3'),(136,'Entry','2017-11-17 09:30:41','2017-11-17 09:30:41','eb4c6cf5-c72a-4cd7-9559-62465a8eed06'),(137,'Asset','2017-11-17 09:57:58','2017-11-17 09:57:58','e38c4c6f-2ed0-4031-8d23-1ab1b0129383'),(138,'Neo_Block','2017-11-20 04:00:41','2017-11-20 04:00:41','ea7febc9-de37-46da-ba76-4a5f7088571b'),(139,'Neo_Block','2017-11-20 04:00:41','2017-11-20 04:00:41','bd24f203-4f50-4a23-8a05-e9f2a29a5b9b'),(140,'Neo_Block','2017-11-20 04:00:41','2017-11-20 04:00:41','9aaea155-45e9-4290-96ae-2f4214d72556'),(141,'Neo_Block','2017-11-20 04:00:41','2017-11-20 04:00:41','8c4b21df-7a27-4efd-9290-dae6e3d40a73');
/*!40000 ALTER TABLE `cr_fieldlayouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_fieldlayouttabs`
--

DROP TABLE IF EXISTS `cr_fieldlayouttabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_fieldlayouttabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  KEY `cr_fieldlayouttabs_layoutId_fk` (`layoutId`),
  CONSTRAINT `cr_fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `cr_fieldlayouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_fieldlayouttabs`
--

LOCK TABLES `cr_fieldlayouttabs` WRITE;
/*!40000 ALTER TABLE `cr_fieldlayouttabs` DISABLE KEYS */;
INSERT INTO `cr_fieldlayouttabs` VALUES (79,117,'Content',1,'2017-11-16 08:11:42','2017-11-16 08:11:42','02c0e2aa-7816-4123-9b7a-7d412c8c52e7'),(95,135,'Content',1,'2017-11-17 07:28:37','2017-11-17 07:28:37','f6020f84-2e39-4c05-bdd1-cb87b7a82969'),(96,136,'Content',1,'2017-11-17 09:30:41','2017-11-17 09:30:41','194d84e2-4b1d-428c-a883-ed8a15b9f2de'),(97,138,'Hero',1,'2017-11-20 04:00:41','2017-11-20 04:00:41','969a6ae2-9caf-42bd-8679-60d6e16f3108'),(98,139,'Cards',1,'2017-11-20 04:00:41','2017-11-20 04:00:41','39dbbe3f-e721-44f1-a34f-87d63f8e49bb'),(99,140,'Card Item',1,'2017-11-20 04:00:41','2017-11-20 04:00:41','2946afc8-a128-4631-a1b7-26129a571e14'),(100,141,'Box Video',1,'2017-11-20 04:00:41','2017-11-20 04:00:41','fe94c2b5-efca-4b6b-a2f4-2634689ad305');
/*!40000 ALTER TABLE `cr_fieldlayouttabs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_fields`
--

DROP TABLE IF EXISTS `cr_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(58) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'global',
  `instructions` text COLLATE utf8_unicode_ci,
  `translatable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_fields_handle_context_unq_idx` (`handle`,`context`),
  KEY `cr_fields_context_idx` (`context`),
  KEY `cr_fields_groupId_fk` (`groupId`),
  CONSTRAINT `cr_fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `cr_fieldgroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_fields`
--

LOCK TABLES `cr_fields` WRITE;
/*!40000 ALTER TABLE `cr_fields` DISABLE KEYS */;
INSERT INTO `cr_fields` VALUES (1,1,'Body','body','global',NULL,1,'RichText','{\"configFile\":\"Standard.json\",\"columnType\":\"text\"}','2017-11-15 02:22:10','2017-11-15 02:22:10','9b3803ed-36e9-46ee-bd5a-ae1c0e4c9bd8'),(2,1,'Tags','tags','global',NULL,0,'Tags','{\"source\":\"taggroup:1\"}','2017-11-15 02:22:10','2017-11-15 02:22:10','99490ec3-4284-4b37-890b-a743b861e024'),(12,2,'Sections','sections','global','',0,'Neo','{\"maxBlocks\":null}','2017-11-15 08:55:40','2017-11-20 04:00:41','b7fe5f20-9705-4f98-8a8a-775e3886aba2'),(14,1,'Image','image','global','',0,'Assets','{\"useSingleFolder\":\"\",\"sources\":[\"folder:1\"],\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}','2017-11-15 09:06:25','2017-11-16 02:37:58','0cd57801-3672-4ca0-9a66-3c949e536157'),(15,1,'Title','heading','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"255\",\"multiline\":\"\",\"initialRows\":\"4\"}','2017-11-15 09:06:57','2017-11-15 09:06:57','1e0d50b7-2df9-4a62-b519-0e796483ae77'),(16,1,'Description','description','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"1\",\"initialRows\":\"3\"}','2017-11-15 09:07:10','2017-11-15 09:07:10','357cfd08-2273-45a5-9ebd-eba37c5a6273'),(17,1,'Video Embed','videoEmbed','global','',0,'Assets','{\"useSingleFolder\":\"\",\"sources\":[\"folder:1\"],\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"json\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}','2017-11-16 03:59:23','2017-11-16 04:08:40','4396ccd3-00ef-4f76-80ed-30befc9d6e06'),(18,1,'Media','media','global','',0,'Assets','{\"useSingleFolder\":\"\",\"sources\":[\"folder:1\"],\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"image\",\"json\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}','2017-11-16 03:59:59','2017-11-16 03:59:59','f87bce9e-bc79-4d8a-a12d-b3c5d724a6e4'),(19,1,'Document','document','global','',0,'Assets','{\"useSingleFolder\":\"\",\"sources\":[\"folder:1\"],\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"1\",\"allowedKinds\":[\"excel\",\"pdf\",\"word\"],\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}','2017-11-16 04:00:49','2017-11-16 04:51:14','6d7d221d-f715-4902-b2c4-4bff1d5c19b5'),(20,1,'Position','position','global','',0,'PositionSelect','{\"options\":[\"left\",\"right\"]}','2017-11-16 04:05:14','2017-11-16 12:29:24','3416dba4-b1a7-433f-bf30-7773bf5e49fd'),(21,1,'Label','label','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"255\",\"multiline\":\"\",\"initialRows\":\"4\"}','2017-11-16 04:07:23','2017-11-16 04:07:23','58d72623-f7eb-40ab-813f-f1db57b8fa04'),(22,1,'Link','linkIt','global','',0,'FruitLinkIt','{\"types\":[\"custom\",\"entry\"],\"defaultText\":\"\",\"allowCustomText\":\"1\",\"allowTarget\":\"\",\"entrySources\":\"*\",\"entrySelectionLabel\":\"Select an entry\",\"assetSources\":\"*\",\"assetSelectionLabel\":\"Select an asset\"}','2017-11-16 08:01:51','2017-11-16 08:02:02','9dc215d8-8ca2-447d-9cc7-3a655fa77c8b'),(23,1,'Color','color','global','',0,'Spectrum_ColorPicker','{\"allowEmpty\":\"1\",\"showInput\":\"1\",\"showAlpha\":\"\",\"showPalette\":\"1\",\"showPaletteOnly\":\"1\",\"preferredFormat\":\"hex\",\"defaultColor\":\"#ffffff\",\"useDefaultPalette\":\"1\"}','2017-11-16 08:18:01','2017-11-16 08:29:30','b4df8ca5-b656-49d6-8fde-7a88a5546775'),(24,1,'Align','align','global','',0,'PositionSelect','{\"options\":[\"left\",\"center\",\"right\"]}','2017-11-16 12:29:12','2017-11-16 12:29:12','bdbdbb64-dbbd-44f6-8320-98e2cc047e46'),(25,1,'Categories','categories','global','',0,'Categories','{\"source\":\"group:1\",\"limit\":\"\",\"selectionLabel\":\"\"}','2017-11-17 07:26:38','2017-11-17 07:26:58','8275a177-b139-499c-93da-7b7a2dfe8558');
/*!40000 ALTER TABLE `cr_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_globalsets`
--

DROP TABLE IF EXISTS `cr_globalsets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_globalsets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_globalsets_name_unq_idx` (`name`),
  UNIQUE KEY `cr_globalsets_handle_unq_idx` (`handle`),
  KEY `cr_globalsets_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `cr_globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `cr_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `cr_globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_globalsets`
--

LOCK TABLES `cr_globalsets` WRITE;
/*!40000 ALTER TABLE `cr_globalsets` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_globalsets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_info`
--

DROP TABLE IF EXISTS `cr_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `edition` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `siteName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `siteUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `maintenance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_info`
--

LOCK TABLES `cr_info` WRITE;
/*!40000 ALTER TABLE `cr_info` DISABLE KEYS */;
INSERT INTO `cr_info` VALUES (1,'2.6.2997','2.6.11',0,'Craftcms','{baseUrl}','UTC',1,0,'2017-11-15 02:22:08','2017-11-17 10:06:53','3309c8da-0d4c-4164-b9a9-432e549621b4');
/*!40000 ALTER TABLE `cr_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_locales`
--

DROP TABLE IF EXISTS `cr_locales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_locales` (
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`locale`),
  KEY `cr_locales_sortOrder_idx` (`sortOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_locales`
--

LOCK TABLES `cr_locales` WRITE;
/*!40000 ALTER TABLE `cr_locales` DISABLE KEYS */;
INSERT INTO `cr_locales` VALUES ('en_us',1,'2017-11-15 02:22:08','2017-11-15 02:22:08','161c5eb6-416d-4b0b-89c3-00dcb97b3c75');
/*!40000 ALTER TABLE `cr_locales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_matrixblocks`
--

DROP TABLE IF EXISTS `cr_matrixblocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `ownerLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_matrixblocks_ownerId_idx` (`ownerId`),
  KEY `cr_matrixblocks_fieldId_idx` (`fieldId`),
  KEY `cr_matrixblocks_typeId_idx` (`typeId`),
  KEY `cr_matrixblocks_sortOrder_idx` (`sortOrder`),
  KEY `cr_matrixblocks_ownerLocale_fk` (`ownerLocale`),
  CONSTRAINT `cr_matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `cr_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_matrixblocks_ownerLocale_fk` FOREIGN KEY (`ownerLocale`) REFERENCES `cr_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cr_matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `cr_matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_matrixblocks`
--

LOCK TABLES `cr_matrixblocks` WRITE;
/*!40000 ALTER TABLE `cr_matrixblocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_matrixblocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_matrixblocktypes`
--

DROP TABLE IF EXISTS `cr_matrixblocktypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_matrixblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  UNIQUE KEY `cr_matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `cr_matrixblocktypes_fieldId_fk` (`fieldId`),
  KEY `cr_matrixblocktypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `cr_matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `cr_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `cr_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_matrixblocktypes`
--

LOCK TABLES `cr_matrixblocktypes` WRITE;
/*!40000 ALTER TABLE `cr_matrixblocktypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_matrixblocktypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_migrations`
--

DROP TABLE IF EXISTS `cr_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pluginId` int(11) DEFAULT NULL,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_migrations_version_unq_idx` (`version`),
  KEY `cr_migrations_pluginId_fk` (`pluginId`),
  CONSTRAINT `cr_migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `cr_plugins` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_migrations`
--

LOCK TABLES `cr_migrations` WRITE;
/*!40000 ALTER TABLE `cr_migrations` DISABLE KEYS */;
INSERT INTO `cr_migrations` VALUES (1,NULL,'m000000_000000_base','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','1e18bffc-7495-4dbc-a864-7819517078d4'),(2,NULL,'m140730_000001_add_filename_and_format_to_transformindex','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','c0c9e3a8-2a44-46b6-812c-860b04836184'),(3,NULL,'m140815_000001_add_format_to_transforms','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','12c9d67a-2d40-472c-ac8b-c25d916c7c7d'),(4,NULL,'m140822_000001_allow_more_than_128_items_per_field','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','a4b1965f-82ec-4440-850c-4fc931579820'),(5,NULL,'m140829_000001_single_title_formats','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','97db34a5-4a9c-4cf4-bee8-f1afcea4a50f'),(6,NULL,'m140831_000001_extended_cache_keys','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','acca3589-c4fe-4d46-8731-39d0f2af6d08'),(7,NULL,'m140922_000001_delete_orphaned_matrix_blocks','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','36363dae-5fa2-433c-ba54-823c9248db28'),(8,NULL,'m141008_000001_elements_index_tune','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','764e4387-a248-4da1-ae02-391f1438b90a'),(9,NULL,'m141009_000001_assets_source_handle','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','76c93683-86a2-4b5b-a887-ce27ad261250'),(10,NULL,'m141024_000001_field_layout_tabs','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','f1ad06de-cba7-4063-922a-955b6bd60427'),(11,NULL,'m141030_000000_plugin_schema_versions','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','ea8a6fd5-e038-4ba0-ab94-b4383718991f'),(12,NULL,'m141030_000001_drop_structure_move_permission','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','ba4b1d13-e71a-40d1-88d3-834aa308865b'),(13,NULL,'m141103_000001_tag_titles','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','c7fc35ee-1ebd-43b1-9ad3-321ed18db944'),(14,NULL,'m141109_000001_user_status_shuffle','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','c0b04cfe-cb63-4ff5-9885-8dc04c9bab0f'),(15,NULL,'m141126_000001_user_week_start_day','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','9fd70c62-06ed-48d7-81e0-45792867c8f7'),(16,NULL,'m150210_000001_adjust_user_photo_size','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','0f688bcb-8e35-4175-8edf-eaee98fe11d6'),(17,NULL,'m150724_000001_adjust_quality_settings','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','1536ac0f-a99e-42fd-82c4-a9c6a11f03b9'),(18,NULL,'m150827_000000_element_index_settings','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','9a7ec7ff-dc3f-4f17-acc5-9f7552f3c9b0'),(19,NULL,'m150918_000001_add_colspan_to_widgets','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','a412bb3d-aadf-4783-b340-a48578f2141d'),(20,NULL,'m151007_000000_clear_asset_caches','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','4d4c4dae-7b9f-40a2-887c-e870d34f7aa1'),(21,NULL,'m151109_000000_text_url_formats','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','f1514cf7-0835-4f0b-8217-231166a8e7c2'),(22,NULL,'m151110_000000_move_logo','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','3b1eb091-f413-4e11-850a-a0a301d0150f'),(23,NULL,'m151117_000000_adjust_image_widthheight','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','2b9ac34c-0e5e-4b15-804b-405408ca0d96'),(24,NULL,'m151127_000000_clear_license_key_status','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','4dfb6636-34da-47cd-8b49-f7928042412e'),(25,NULL,'m151127_000000_plugin_license_keys','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','2e32b8cc-a079-4d62-bf0b-116c7e2f70fc'),(26,NULL,'m151130_000000_update_pt_widget_feeds','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','8890b935-8834-4d99-8a0e-5b3a0989cd1c'),(27,NULL,'m160114_000000_asset_sources_public_url_default_true','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','16b8cbc3-dccc-425f-b967-56d522a95022'),(28,NULL,'m160223_000000_sortorder_to_smallint','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','bee5c64f-e7ad-4da0-8c0b-17b086712703'),(29,NULL,'m160229_000000_set_default_entry_statuses','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','67a00cec-b31f-4aad-8785-32c1957bed47'),(30,NULL,'m160304_000000_client_permissions','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','66801808-29c3-4ebe-bea6-72e3289a4bfe'),(31,NULL,'m160322_000000_asset_filesize','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','fb266414-07d8-4240-b9eb-f5eeac07ddd9'),(32,NULL,'m160503_000000_orphaned_fieldlayouts','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','cb05d854-8638-4073-af73-5f69d74b2591'),(33,NULL,'m160510_000000_tasksettings','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','5efb62f2-789b-4e8e-8af8-5c73dd4a20b4'),(34,NULL,'m160829_000000_pending_user_content_cleanup','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','d2ba1d68-6775-450e-baf5-fa152590c1f5'),(35,NULL,'m160830_000000_asset_index_uri_increase','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','dd942441-f7a7-4e7d-8278-99ab5a7570d3'),(36,NULL,'m160919_000000_usergroup_handle_title_unique','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','bb29fdbe-fe91-42df-b6cd-7541f0b81f14'),(37,NULL,'m161108_000000_new_version_format','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','33e9967a-2748-4709-8993-c6465040fe62'),(38,NULL,'m161109_000000_index_shuffle','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','605ff382-fc09-4c25-8637-51b3bbe5575c'),(39,NULL,'m170612_000000_route_index_shuffle','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','3a355216-ebce-4360-ac04-62cad6f154d2'),(40,NULL,'m171107_000000_assign_group_permissions','2017-11-15 02:22:08','2017-11-15 02:22:08','2017-11-15 02:22:08','5ad467bc-dcde-4311-b996-2273a4f634fd'),(41,1,'m160428_202308_Neo_UpdateBlockLevels','2017-11-15 08:49:21','2017-11-15 08:49:21','2017-11-15 08:49:21','e275c514-9371-4a02-9404-b9789c968f60'),(42,1,'m160515_005002_Neo_UpdateBlockStructure','2017-11-15 08:49:21','2017-11-15 08:49:21','2017-11-15 08:49:21','664d8d69-aaca-4d53-a4d0-f3a2bc559a24'),(43,1,'m160605_191540_Neo_UpdateBlockStructureLocales','2017-11-15 08:49:21','2017-11-15 08:49:21','2017-11-15 08:49:21','cc642ada-9073-475c-80cd-353b0b32d6c2'),(44,1,'m161029_230849_Neo_AddMaxChildBlocksSetting','2017-11-15 08:49:21','2017-11-15 08:49:21','2017-11-15 08:49:21','68a2d2a6-60fb-499f-b26b-8dc396f019be'),(45,5,'m150212_145000_AmNav_renamePagesToNodes','2017-11-16 04:56:13','2017-11-16 04:56:13','2017-11-16 04:56:13','54c3aec7-4e66-4672-b877-fdffab658646'),(46,5,'m150217_112800_AmNav_expandPageData','2017-11-16 04:56:13','2017-11-16 04:56:13','2017-11-16 04:56:13','b8df8db0-4f10-47e3-a18a-286106562c92'),(47,5,'m150403_093000_AmNav_nodesWithElements','2017-11-16 04:56:13','2017-11-16 04:56:13','2017-11-16 04:56:13','effc3235-747a-4e43-9724-9f77a0d39193'),(48,5,'m150512_105600_AmNav_addOptionalClass','2017-11-16 04:56:13','2017-11-16 04:56:13','2017-11-16 04:56:13','dc08ff65-14c0-4a83-a1dd-6b326072c00b'),(49,6,'m160208_010101_FruitLinkIt_UpdateExistingLinkItFields','2017-11-16 07:59:20','2017-11-16 07:59:20','2017-11-16 07:59:20','e0f464db-d91b-4573-8a9f-a4ff0521de41');
/*!40000 ALTER TABLE `cr_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_neoblocks`
--

DROP TABLE IF EXISTS `cr_neoblocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_neoblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `collapsed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ownerLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_neoblocks_ownerId_idx` (`ownerId`),
  KEY `cr_neoblocks_fieldId_idx` (`fieldId`),
  KEY `cr_neoblocks_typeId_idx` (`typeId`),
  KEY `cr_neoblocks_collapsed_idx` (`collapsed`),
  KEY `cr_neoblocks_ownerLocale_fk` (`ownerLocale`),
  CONSTRAINT `cr_neoblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `cr_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_neoblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_neoblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_neoblocks_ownerLocale_fk` FOREIGN KEY (`ownerLocale`) REFERENCES `cr_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cr_neoblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `cr_neoblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_neoblocks`
--

LOCK TABLES `cr_neoblocks` WRITE;
/*!40000 ALTER TABLE `cr_neoblocks` DISABLE KEYS */;
INSERT INTO `cr_neoblocks` VALUES (6,5,12,7,0,NULL,'2017-11-16 02:37:21','2017-11-16 12:31:27','ae7a2b24-2d34-4543-96b0-8a03ed283418'),(13,5,12,12,0,NULL,'2017-11-16 04:10:42','2017-11-16 12:31:27','70f23a57-2196-41a5-a266-6c9542b56790'),(14,5,12,6,0,NULL,'2017-11-16 10:06:05','2017-11-16 12:31:27','b6a50427-ef4d-40e4-9574-9557d6fc716f'),(15,5,12,13,0,NULL,'2017-11-16 10:06:05','2017-11-16 12:31:27','594f9e62-13d1-4d05-ba04-e3d24124b615'),(16,5,12,13,0,NULL,'2017-11-16 10:06:05','2017-11-16 12:31:27','d02e53d2-5079-451a-b5e9-c59eb2d69990'),(17,5,12,13,0,NULL,'2017-11-16 10:06:06','2017-11-16 12:31:27','3362ef33-e4ee-46cf-b82c-1abaa9d4c739'),(18,2,12,7,0,NULL,'2017-11-17 09:31:30','2017-11-17 09:31:30','5f5786bc-4e92-4be1-8dd1-918a1ab9ecf0');
/*!40000 ALTER TABLE `cr_neoblocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_neoblockstructures`
--

DROP TABLE IF EXISTS `cr_neoblockstructures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_neoblockstructures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `ownerLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_neoblockstructures_structureId_idx` (`structureId`),
  KEY `cr_neoblockstructures_ownerId_idx` (`ownerId`),
  KEY `cr_neoblockstructures_fieldId_idx` (`fieldId`),
  KEY `cr_neoblockstructures_ownerLocale_fk` (`ownerLocale`),
  CONSTRAINT `cr_neoblockstructures_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `cr_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_neoblockstructures_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_neoblockstructures_ownerLocale_fk` FOREIGN KEY (`ownerLocale`) REFERENCES `cr_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cr_neoblockstructures_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `cr_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_neoblockstructures`
--

LOCK TABLES `cr_neoblockstructures` WRITE;
/*!40000 ALTER TABLE `cr_neoblockstructures` DISABLE KEYS */;
INSERT INTO `cr_neoblockstructures` VALUES (10,11,5,12,NULL,'2017-11-16 12:31:27','2017-11-16 12:31:27','79e5c037-9132-474d-a480-217eda2028fe'),(11,13,2,12,NULL,'2017-11-17 09:31:30','2017-11-17 09:31:30','b4d395b7-cdd7-4c6c-adf9-e015dec1fc6a');
/*!40000 ALTER TABLE `cr_neoblockstructures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_neoblocktypes`
--

DROP TABLE IF EXISTS `cr_neoblocktypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_neoblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `maxBlocks` int(10) DEFAULT '0',
  `maxChildBlocks` int(10) DEFAULT '0',
  `childBlocks` text COLLATE utf8_unicode_ci,
  `topLevel` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_neoblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  UNIQUE KEY `cr_neoblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `cr_neoblocktypes_fieldId_fk` (`fieldId`),
  KEY `cr_neoblocktypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `cr_neoblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `cr_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_neoblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `cr_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_neoblocktypes`
--

LOCK TABLES `cr_neoblocktypes` WRITE;
/*!40000 ALTER TABLE `cr_neoblocktypes` DISABLE KEYS */;
INSERT INTO `cr_neoblocktypes` VALUES (6,12,139,'Cards','cards',NULL,NULL,'[\"cardItem\"]',1,2,'2017-11-15 09:12:48','2017-11-20 04:00:41','a7db7477-a9c2-4088-8880-85f564aa9229'),(7,12,138,'Hero','hero',NULL,NULL,'',1,1,'2017-11-15 09:22:28','2017-11-20 04:00:41','4de163f5-7c74-49a0-974d-cc5d2d1efff1'),(12,12,141,'Box Video','boxVideo',NULL,NULL,'',1,4,'2017-11-16 04:04:19','2017-11-20 04:00:41','f6217e34-4653-4749-8318-c628e9e61bca'),(13,12,140,'Card Item','cardItem',NULL,NULL,'',0,3,'2017-11-16 07:54:19','2017-11-20 04:00:41','a789544d-6b1f-476b-90ed-d050a8275dd6');
/*!40000 ALTER TABLE `cr_neoblocktypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_neogroups`
--

DROP TABLE IF EXISTS `cr_neogroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_neogroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_neogroups_fieldId_fk` (`fieldId`),
  CONSTRAINT `cr_neogroups_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `cr_fields` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_neogroups`
--

LOCK TABLES `cr_neogroups` WRITE;
/*!40000 ALTER TABLE `cr_neogroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_neogroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_plugins`
--

DROP TABLE IF EXISTS `cr_plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKey` char(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','unknown') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `settings` text COLLATE utf8_unicode_ci,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_plugins`
--

LOCK TABLES `cr_plugins` WRITE;
/*!40000 ALTER TABLE `cr_plugins` DISABLE KEYS */;
INSERT INTO `cr_plugins` VALUES (1,'Neo','1.5.0','1.5.0',NULL,'unknown',1,NULL,'2017-11-15 08:49:21','2017-11-15 08:49:21','2017-11-20 02:41:10','efbb295c-ee48-4592-bec7-023bbe19f47f'),(3,'Relabel','0.1.3','1.0.0',NULL,'unknown',1,NULL,'2017-11-15 10:44:52','2017-11-15 10:44:52','2017-11-20 02:41:10','c428fd34-ecd0-415a-9a90-e41375782dc8'),(4,'EmbeddedAssets','0.4.1','0.0.1',NULL,'unknown',1,'{\"whitelist\":[\"vimeo.com\",\"youtu.be\",\"youtube.com\",\"youtube-nocookie.com\"],\"parameters\":{\"maxwidth\":\"1280\",\"maxheight\":\"960\"}}','2017-11-16 03:49:53','2017-11-16 03:49:53','2017-11-20 02:41:10','37afb4ab-792c-494d-b8ef-cfd8de0aaeb5'),(5,'AmNav','1.8.0','1.7.4',NULL,'unknown',1,'{\"pluginName\":\"Navigation\",\"canDoActions\":\"1\",\"quietErrors\":\"\"}','2017-11-16 04:56:13','2017-11-16 04:56:13','2017-11-20 02:41:10','b936cf63-d5b3-4447-b85b-6cfdb576353e'),(6,'FruitLinkIt','2.3.4','2.3.0',NULL,'unknown',1,NULL,'2017-11-16 07:59:20','2017-11-16 07:59:20','2017-11-20 02:41:10','5a3c7177-da4c-4215-9138-d2639823b154'),(7,'Spectrum','0.10.2',NULL,NULL,'unknown',1,'{\"palette\":{\"2\":{\"color\":\"#ffffff\"},\"0\":{\"color\":\"#112146\"}}}','2017-11-16 08:24:26','2017-11-16 08:24:26','2017-11-20 02:41:10','13259c49-6ece-4abc-8e1a-ea34616f5cf0'),(8,'Imager','1.6.4','1.0.0',NULL,'unknown',1,NULL,'2017-11-16 11:27:37','2017-11-16 11:27:37','2017-11-20 02:41:10','3b73c7cd-77f7-4446-887d-88ec3aa3a1e0'),(9,'FfwCustom','0.0.1',NULL,NULL,'unknown',1,NULL,'2017-11-17 02:30:10','2017-11-17 02:30:10','2017-11-20 02:41:10','6e7343d6-8cd8-47eb-bd76-7bb7865f9ea1');
/*!40000 ALTER TABLE `cr_plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_rackspaceaccess`
--

DROP TABLE IF EXISTS `cr_rackspaceaccess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_rackspaceaccess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connectionKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `storageUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cdnUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_rackspaceaccess_connectionKey_unq_idx` (`connectionKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_rackspaceaccess`
--

LOCK TABLES `cr_rackspaceaccess` WRITE;
/*!40000 ALTER TABLE `cr_rackspaceaccess` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_rackspaceaccess` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_relabel`
--

DROP TABLE IF EXISTS `cr_relabel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_relabel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) DEFAULT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instructions` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_relabel_fieldId_fk` (`fieldId`),
  KEY `cr_relabel_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `cr_relabel_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `cr_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_relabel_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `cr_fieldlayouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_relabel`
--

LOCK TABLES `cr_relabel` WRITE;
/*!40000 ALTER TABLE `cr_relabel` DISABLE KEYS */;
INSERT INTO `cr_relabel` VALUES (37,14,117,'Image featured',NULL,'2017-11-16 08:11:42','2017-11-16 08:11:42','6a2df19a-df00-42d8-b011-c87f2860037d'),(64,16,135,'Teaser',NULL,'2017-11-17 07:28:37','2017-11-17 07:28:37','3ff3593d-67ea-44c4-af19-2bd1e7a24211'),(65,24,138,'Content Align',NULL,'2017-11-20 04:00:41','2017-11-20 04:00:41','f02ffb15-e29a-4072-a668-ebfc53e45651'),(66,14,138,'Background Image',NULL,'2017-11-20 04:00:41','2017-11-20 04:00:41','75429960-fffe-4230-aa4b-7a53e7a1cad1'),(67,20,138,'Content Align',NULL,'2017-11-20 04:00:41','2017-11-20 04:00:41','4327dbcc-3f46-4998-8950-327ac4490056'),(68,23,139,'Background Color',NULL,'2017-11-20 04:00:41','2017-11-20 04:00:41','2905323d-f913-4533-bebf-4b5f7d80784e'),(69,23,141,'Background Color',NULL,'2017-11-20 04:00:41','2017-11-20 04:00:41','441ba811-8759-4865-8b40-4c4d2edb53d4'),(70,21,141,'Video Title',NULL,'2017-11-20 04:00:41','2017-11-20 04:00:41','b8973ccd-98ef-4d61-9f7e-4ef8a283d1be'),(71,16,141,'Video Description',NULL,'2017-11-20 04:00:41','2017-11-20 04:00:41','c5624bd5-88ae-4b68-8c65-e8cf4671eea5'),(72,20,141,'Video Position',NULL,'2017-11-20 04:00:41','2017-11-20 04:00:41','7eb607c7-74b2-4292-82d9-e0795798d644');
/*!40000 ALTER TABLE `cr_relabel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_relations`
--

DROP TABLE IF EXISTS `cr_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_relations_fieldId_sourceId_sourceLocale_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceLocale`,`targetId`),
  KEY `cr_relations_sourceId_fk` (`sourceId`),
  KEY `cr_relations_sourceLocale_fk` (`sourceLocale`),
  KEY `cr_relations_targetId_fk` (`targetId`),
  CONSTRAINT `cr_relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `cr_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_relations_sourceLocale_fk` FOREIGN KEY (`sourceLocale`) REFERENCES `cr_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cr_relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_relations`
--

LOCK TABLES `cr_relations` WRITE;
/*!40000 ALTER TABLE `cr_relations` DISABLE KEYS */;
INSERT INTO `cr_relations` VALUES (23,14,6,NULL,4,1,'2017-11-16 12:31:27','2017-11-16 12:31:27','17d64b13-b7af-4d18-abe0-8388a93878ba'),(24,17,13,NULL,12,1,'2017-11-16 12:31:27','2017-11-16 12:31:27','28f31b9f-4d66-4b5a-9bd9-3afbd60d32bf'),(25,14,15,NULL,7,1,'2017-11-16 12:31:27','2017-11-16 12:31:27','5bd45047-5f86-4dbd-a879-8d8185a80611'),(26,14,16,NULL,8,1,'2017-11-16 12:31:27','2017-11-16 12:31:27','6f0d02b9-f568-4153-ac89-39123823fd06'),(27,14,17,NULL,4,1,'2017-11-16 12:31:27','2017-11-16 12:31:27','643cb7bd-eee9-42b7-90e1-1fb0972d9a2c'),(32,18,19,NULL,12,1,'2017-11-20 03:17:35','2017-11-20 03:17:35','3563e6b5-7022-4157-ae4a-acf92fed7443'),(33,2,19,NULL,20,1,'2017-11-20 03:17:35','2017-11-20 03:17:35','a5632b5e-1dcf-4bef-96b1-8e86d1bf3ccc'),(34,2,19,NULL,21,2,'2017-11-20 03:17:35','2017-11-20 03:17:35','7bff43f0-7a3f-4edf-9d11-4ab1a09ba381'),(35,18,3,NULL,4,1,'2017-11-20 03:17:56','2017-11-20 03:17:56','854a465a-c7b3-447e-a636-a738d9f69bfe'),(36,2,3,NULL,20,1,'2017-11-20 03:17:56','2017-11-20 03:17:56','46275a53-1c86-439d-a4f2-d6d38d52ab50');
/*!40000 ALTER TABLE `cr_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_routes`
--

DROP TABLE IF EXISTS `cr_routes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `urlParts` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `urlPattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_routes_locale_idx` (`locale`),
  KEY `cr_routes_urlPattern_idx` (`urlPattern`),
  CONSTRAINT `cr_routes_locale_fk` FOREIGN KEY (`locale`) REFERENCES `cr_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_routes`
--

LOCK TABLES `cr_routes` WRITE;
/*!40000 ALTER TABLE `cr_routes` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_routes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_searchindex`
--

DROP TABLE IF EXISTS `cr_searchindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `fieldId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`locale`),
  FULLTEXT KEY `cr_searchindex_keywords_idx` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_searchindex`
--

LOCK TABLES `cr_searchindex` WRITE;
/*!40000 ALTER TABLE `cr_searchindex` DISABLE KEYS */;
INSERT INTO `cr_searchindex` VALUES (1,'username',0,'en_us',' admin '),(1,'firstname',0,'en_us',''),(1,'lastname',0,'en_us',''),(1,'fullname',0,'en_us',''),(1,'email',0,'en_us',' chuyen luu ffwagency com '),(1,'slug',0,'en_us',''),(2,'slug',0,'en_us',' homepage '),(2,'title',0,'en_us',' welcome to craft '),(2,'field',1,'en_us',' it s true this site doesn t have a whole lot of content yet but don t worry our web developers have just installed the cms and they re setting things up for the content editors this very moment soon craftcms docksal will be an oasis of fresh perspectives sharp analyses and astute opinions that will keep you coming back again and again '),(3,'field',1,'en_us',' craft is the cms that s powering craftcms docksal it s beautiful powerful flexible and easy to use and it s made by pixel tonic we can t wait to dive in and see what it s capable of this is even more captivating content which you couldn t see on the news index page because it was entered after a page break and the news index template only likes to show the content on the first page craft a nice alternative to word if you re making a website '),(3,'field',2,'en_us',' craft '),(3,'slug',0,'en_us',' we just installed craft '),(3,'title',0,'en_us',' we just installed craft '),(13,'field',16,'en_us',' see how your content will look before it goes live with live preview '),(13,'field',21,'en_us',' craft previewing '),(13,'field',17,'en_us',' content previewing '),(12,'title',0,'en_us',' content previewing '),(12,'slug',0,'en_us',' embed content previewing '),(12,'kind',0,'en_us',' json '),(14,'slug',0,'en_us',''),(13,'field',23,'en_us',' ffffff '),(12,'filename',0,'en_us',' embed_content previewing json '),(12,'extension',0,'en_us',' json '),(15,'field',14,'en_us',' example 1 '),(15,'field',21,'en_us',' craftcms '),(15,'field',15,'en_us',' example card 1 '),(15,'field',16,'en_us',' curabitur non nulla sit amet nisl tempus convallis quis ac lectus '),(15,'field',22,'en_us',' entry 2 read more '),(8,'extension',0,'en_us',' jpg '),(8,'kind',0,'en_us',' image '),(8,'slug',0,'en_us',' example 2 '),(8,'title',0,'en_us',' example 2 '),(8,'filename',0,'en_us',' example 2 jpg '),(7,'title',0,'en_us',' example 1 '),(7,'slug',0,'en_us',' example 1 '),(7,'kind',0,'en_us',' image '),(7,'extension',0,'en_us',' jpg '),(7,'filename',0,'en_us',' example 1 jpg '),(6,'field',14,'en_us',' example 3 '),(6,'field',15,'en_us',' example page landing '),(6,'field',16,'en_us',' this is example block type '),(6,'slug',0,'en_us',''),(5,'title',0,'en_us',' example page landing '),(5,'slug',0,'en_us',' example page landing '),(5,'field',12,'en_us',''),(5,'field',14,'en_us',''),(4,'title',0,'en_us',' example 3 '),(4,'filename',0,'en_us',' example 3 jpg '),(4,'extension',0,'en_us',' jpg '),(4,'kind',0,'en_us',' image '),(4,'slug',0,'en_us',' example 3 '),(13,'field',20,'en_us',' left '),(13,'field',15,'en_us',' preview while you work '),(13,'field',1,'en_us',' proin eget tortor risus curabitur non nulla sit amet nisl tempus convallis quis ac lectus nulla porttitor accumsan tincidunt vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui vivamus suscipit tortor eget felis porttitor volutpat vivamus magna justo lacinia eget consectetur sed convallis at tellus pellentesque in ipsum id orci porta dapibus praesent sapien massa convallis a pellentesque nec egestas non nisi sed porttitor lectus nibh nulla porttitor accumsan tincidunt '),(13,'slug',0,'en_us',''),(15,'slug',0,'en_us',''),(16,'field',14,'en_us',' example 2 '),(16,'field',21,'en_us',' template '),(16,'field',15,'en_us',' example card 2 '),(16,'field',16,'en_us',' curabitur non nulla sit amet nisl tempus convallis quis ac lectus '),(16,'field',22,'en_us',' custom http google com read more '),(16,'slug',0,'en_us',''),(17,'field',14,'en_us',' example 3 '),(17,'field',21,'en_us',' integration '),(17,'field',15,'en_us',' example card 3 '),(17,'field',16,'en_us',' curabitur non nulla sit amet nisl tempus convallis quis ac lectus '),(17,'field',22,'en_us',' custom http craftcms com read more '),(17,'slug',0,'en_us',''),(6,'field',20,'en_us',' center '),(14,'field',23,'en_us',' ffffff '),(6,'field',24,'en_us',' center '),(3,'field',18,'en_us',' example 3 '),(3,'field',16,'en_us',' craft is the cms that s powering craftcms docksal it s beautiful powerful flexible and easy to use and it s made by pixel tonic '),(3,'field',25,'en_us',''),(2,'field',12,'en_us',''),(18,'field',24,'en_us',' center '),(18,'field',14,'en_us',''),(18,'field',15,'en_us',' welcome to craft '),(18,'field',16,'en_us',''),(18,'slug',0,'en_us',''),(19,'field',18,'en_us',' content previewing '),(19,'field',16,'en_us',' the teaser of news '),(19,'field',25,'en_us',''),(19,'field',1,'en_us',' content of example news '),(19,'field',2,'en_us',' craft demo '),(19,'slug',0,'en_us',' news video '),(19,'title',0,'en_us',' news video '),(20,'name',0,'en_us',' craft '),(20,'slug',0,'en_us',' craft '),(20,'title',0,'en_us',' craft '),(21,'name',0,'en_us',' demo '),(21,'slug',0,'en_us',' demo '),(21,'title',0,'en_us',' demo ');
/*!40000 ALTER TABLE `cr_searchindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_sections`
--

DROP TABLE IF EXISTS `cr_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('single','channel','structure') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'channel',
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enableVersioning` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_sections_name_unq_idx` (`name`),
  UNIQUE KEY `cr_sections_handle_unq_idx` (`handle`),
  KEY `cr_sections_structureId_fk` (`structureId`),
  CONSTRAINT `cr_sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `cr_structures` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_sections`
--

LOCK TABLES `cr_sections` WRITE;
/*!40000 ALTER TABLE `cr_sections` DISABLE KEYS */;
INSERT INTO `cr_sections` VALUES (1,NULL,'Homepage','homepage','single',1,'index',1,'2017-11-15 02:22:10','2017-11-15 02:22:10','ad2b0987-5ae7-48bd-9bcd-493514c61a90'),(2,NULL,'News','news','channel',1,'news/_entry',1,'2017-11-15 02:22:10','2017-11-15 02:22:10','2d9406e8-a79c-4ec5-9d40-97e08a06e363'),(3,1,'Page','page','structure',1,'page/_entry',1,'2017-11-15 02:55:18','2017-11-15 10:40:08','cd0ef49a-e8ab-4611-975b-51ef0f8f649b');
/*!40000 ALTER TABLE `cr_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_sections_i18n`
--

DROP TABLE IF EXISTS `cr_sections_i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_sections_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `enabledByDefault` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_sections_i18n_sectionId_locale_unq_idx` (`sectionId`,`locale`),
  KEY `cr_sections_i18n_locale_fk` (`locale`),
  CONSTRAINT `cr_sections_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `cr_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cr_sections_i18n_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `cr_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_sections_i18n`
--

LOCK TABLES `cr_sections_i18n` WRITE;
/*!40000 ALTER TABLE `cr_sections_i18n` DISABLE KEYS */;
INSERT INTO `cr_sections_i18n` VALUES (1,1,'en_us',1,'__home__',NULL,'2017-11-15 02:22:10','2017-11-15 02:22:10','d162831d-1519-4658-bc17-d51257131bfa'),(2,2,'en_us',1,'news/{postDate.year}/{slug}',NULL,'2017-11-15 02:22:10','2017-11-15 02:22:10','234ff670-b4b7-4fb8-9578-3b3c4d8aff82'),(3,3,'en_us',0,'{slug}','{parent.uri}/{slug}','2017-11-15 02:55:18','2017-11-15 10:14:59','2f0e2ce1-cbe9-4cb2-8d82-173847a7b8b6');
/*!40000 ALTER TABLE `cr_sections_i18n` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_sessions`
--

DROP TABLE IF EXISTS `cr_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `token` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_sessions_uid_idx` (`uid`),
  KEY `cr_sessions_token_idx` (`token`),
  KEY `cr_sessions_dateUpdated_idx` (`dateUpdated`),
  KEY `cr_sessions_userId_fk` (`userId`),
  CONSTRAINT `cr_sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `cr_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_sessions`
--

LOCK TABLES `cr_sessions` WRITE;
/*!40000 ALTER TABLE `cr_sessions` DISABLE KEYS */;
INSERT INTO `cr_sessions` VALUES (1,1,'c261ba9b803da3f3dc9c6cb8b8a7a54c89d98c70czozMjoiMklRRk4xaUZWUm5OWFpjeUprY0J6anJOcXcxbmVJTkUiOw==','2017-11-15 02:22:10','2017-11-15 02:22:10','b8c97bbf-9ea2-461d-9467-30c42baf07a1'),(2,1,'f89f5e4172e8053f1567145b35f9846fbf580a11czozMjoibVZJfkRocjBhQTRLUW9UZWNRSEYydHZZVmtueVBFREUiOw==','2017-11-15 08:41:10','2017-11-15 08:41:10','818ec5dc-f016-4368-b462-c96075c7c4dd'),(3,1,'04f1b170f844595fc2a820670ae311d8f10359fbczozMjoibkQ5ZTJBRnE3MF9uNmdfV2NTa3hsN2VUQWIyWWVlcnIiOw==','2017-11-16 02:02:03','2017-11-16 02:02:03','b4a4008e-257a-40c8-a561-741ba55c195d'),(4,1,'9690f8a80f3a4ef617a5af3749aaa329baa1ec1fczozMjoidHB2N1ptV1o4MzlxN19Xc3RGUzZLTXI4d1N6QVJBeUUiOw==','2017-11-16 07:11:33','2017-11-16 07:11:33','ddd9faed-a304-4904-a71a-9cf90da0f3a5'),(5,1,'586febf5a2a496fa3bd88689278fb49c4d4ab4d8czozMjoiX2JBcXFTMVhsTUppSkI4SnBTTkhVWkk0cVMxZU1WMjYiOw==','2017-11-16 09:42:27','2017-11-16 09:42:27','b16f1dd2-82d5-4590-a7f8-4f78c5255965'),(6,1,'9a592adcf3f994d55586e2e36c32255100715d1fczozMjoiZ0lZcDQybzBZd1laRzB1NU5EZE42WnoyUDNCQU9VM1MiOw==','2017-11-17 02:26:26','2017-11-17 02:26:26','81b82bce-9d3a-4fe4-825e-2fe680bb895d'),(7,1,'d432ec090eb718ea394b4b9af6ef7e910f79d73bczozMjoiYjNMX28za0Y1M2ZOfk1oaUI2cXhPYXlSdWM0anZNYksiOw==','2017-11-17 03:54:18','2017-11-17 03:54:18','742bde50-f95a-41d1-be98-11e304069301'),(8,1,'fb08796260ae3c55aeae934db98ccb3969cc98d9czozMjoiY1FuaUIxRllMRn5CM2dTb0haM0RXV35ZQUdub3hodnUiOw==','2017-11-17 07:23:46','2017-11-17 07:23:46','03c926a4-1e81-4b92-9dd2-41ab5d7add18'),(9,1,'aa259153579a0ae7074c5b179a24cb3c99aade31czozMjoiQVBTN1ZzZTNmQWRwYW03aUlnZzN6SkVIcnNzMThyaWkiOw==','2017-11-18 04:46:59','2017-11-18 04:46:59','6aac3e5e-ffac-4da3-8fdf-8bf687f29751'),(10,1,'8f1c0c3a951e626f85d9497c41524eab4fca7793czozMjoicXVTY0NJMXpGTVhTWVhGOURGOHh0MnZiUDgxM0pXZXUiOw==','2017-11-20 02:38:07','2017-11-20 02:38:07','edc64c71-b71f-459e-87a6-5e3555f181f8');
/*!40000 ALTER TABLE `cr_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_shunnedmessages`
--

DROP TABLE IF EXISTS `cr_shunnedmessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_shunnedmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_shunnedmessages_userId_message_unq_idx` (`userId`,`message`),
  CONSTRAINT `cr_shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `cr_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_shunnedmessages`
--

LOCK TABLES `cr_shunnedmessages` WRITE;
/*!40000 ALTER TABLE `cr_shunnedmessages` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_shunnedmessages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_structureelements`
--

DROP TABLE IF EXISTS `cr_structureelements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_structureelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  KEY `cr_structureelements_root_idx` (`root`),
  KEY `cr_structureelements_lft_idx` (`lft`),
  KEY `cr_structureelements_rgt_idx` (`rgt`),
  KEY `cr_structureelements_level_idx` (`level`),
  KEY `cr_structureelements_elementId_fk` (`elementId`),
  CONSTRAINT `cr_structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `cr_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_structureelements`
--

LOCK TABLES `cr_structureelements` WRITE;
/*!40000 ALTER TABLE `cr_structureelements` DISABLE KEYS */;
INSERT INTO `cr_structureelements` VALUES (3,1,NULL,3,1,4,0,'2017-11-16 02:37:21','2017-11-16 02:37:21','f4d0c490-60ec-44ff-8153-be190419d8bd'),(4,1,5,3,2,3,1,'2017-11-16 02:37:21','2017-11-16 02:37:21','08a943da-c499-465b-827c-7094b0f09aef'),(39,11,NULL,39,1,14,0,'2017-11-16 12:31:27','2017-11-16 12:31:27','44ffeb7d-aa5a-4e9a-961f-866b1a3139cd'),(40,11,6,39,2,3,1,'2017-11-16 12:31:27','2017-11-16 12:31:27','6df08eda-0b5a-4c8b-9f92-58db318a1bc1'),(41,11,13,39,4,5,1,'2017-11-16 12:31:27','2017-11-16 12:31:27','34b03cb3-ae06-4f1b-a7fc-72fe2e0f132d'),(42,11,14,39,6,13,1,'2017-11-16 12:31:27','2017-11-16 12:31:27','38c09fb2-a7e0-4065-b012-49fa279452b8'),(43,11,15,39,7,8,2,'2017-11-16 12:31:27','2017-11-16 12:31:27','33f7f421-9f4c-4ae4-9814-e340fb4ec732'),(44,11,16,39,9,10,2,'2017-11-16 12:31:27','2017-11-16 12:31:27','0635dc6e-65e5-4c42-abe3-97d288f3cb46'),(45,11,17,39,11,12,2,'2017-11-16 12:31:27','2017-11-16 12:31:27','f994a5f5-5aec-423a-a33f-93d788230892'),(46,13,NULL,46,1,4,0,'2017-11-17 09:31:30','2017-11-17 09:31:30','e44a3f7c-6e16-4795-a188-cd456ebef5c0'),(47,13,18,46,2,3,1,'2017-11-17 09:31:30','2017-11-17 09:31:30','bc5dfc6e-a27f-48a8-865e-f2ee7cc4d2e6');
/*!40000 ALTER TABLE `cr_structureelements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_structures`
--

DROP TABLE IF EXISTS `cr_structures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxLevels` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_structures`
--

LOCK TABLES `cr_structures` WRITE;
/*!40000 ALTER TABLE `cr_structures` DISABLE KEYS */;
INSERT INTO `cr_structures` VALUES (1,NULL,'2017-11-15 02:55:18','2017-11-15 10:40:08','7d0089be-940d-4dd1-93ee-7d879d58e320'),(11,NULL,'2017-11-16 12:31:27','2017-11-16 12:31:27','7bcce4c7-3425-428e-8315-de2667d438e5'),(12,NULL,'2017-11-17 07:26:08','2017-11-17 07:26:08','5584afb9-c9ab-432e-8cb8-b96e7bcf441f'),(13,NULL,'2017-11-17 09:31:30','2017-11-17 09:31:30','a33ded4b-662d-4a02-9393-b9498def6de9');
/*!40000 ALTER TABLE `cr_structures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_systemsettings`
--

DROP TABLE IF EXISTS `cr_systemsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_systemsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_systemsettings_category_unq_idx` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_systemsettings`
--

LOCK TABLES `cr_systemsettings` WRITE;
/*!40000 ALTER TABLE `cr_systemsettings` DISABLE KEYS */;
INSERT INTO `cr_systemsettings` VALUES (1,'email','{\"protocol\":\"php\",\"emailAddress\":\"chuyen.luu@ffwagency.com\",\"senderName\":\"Craftcms\"}','2017-11-15 02:22:10','2017-11-15 02:22:10','c87845dc-7a1b-41a4-ab62-66a0143fc963');
/*!40000 ALTER TABLE `cr_systemsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_taggroups`
--

DROP TABLE IF EXISTS `cr_taggroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_taggroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_taggroups_name_unq_idx` (`name`),
  UNIQUE KEY `cr_taggroups_handle_unq_idx` (`handle`),
  KEY `cr_taggroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `cr_taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `cr_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_taggroups`
--

LOCK TABLES `cr_taggroups` WRITE;
/*!40000 ALTER TABLE `cr_taggroups` DISABLE KEYS */;
INSERT INTO `cr_taggroups` VALUES (1,'Default','default',1,'2017-11-15 02:22:10','2017-11-15 02:22:10','9a3c9dee-11c6-4698-86f5-b4ac178a9c8e');
/*!40000 ALTER TABLE `cr_taggroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_tags`
--

DROP TABLE IF EXISTS `cr_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_tags_groupId_fk` (`groupId`),
  CONSTRAINT `cr_tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `cr_taggroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_tags_id_fk` FOREIGN KEY (`id`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_tags`
--

LOCK TABLES `cr_tags` WRITE;
/*!40000 ALTER TABLE `cr_tags` DISABLE KEYS */;
INSERT INTO `cr_tags` VALUES (20,1,'2017-11-20 03:17:23','2017-11-20 03:17:23','8aeb0b9b-5ef1-456d-83f5-a0b7c6e7c897'),(21,1,'2017-11-20 03:17:33','2017-11-20 03:17:33','14126d2b-8c61-4ffe-b2ed-d171df019b0c');
/*!40000 ALTER TABLE `cr_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_tasks`
--

DROP TABLE IF EXISTS `cr_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `currentStep` int(11) unsigned DEFAULT NULL,
  `totalSteps` int(11) unsigned DEFAULT NULL,
  `status` enum('pending','error','running') COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settings` mediumtext COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_tasks_root_idx` (`root`),
  KEY `cr_tasks_lft_idx` (`lft`),
  KEY `cr_tasks_rgt_idx` (`rgt`),
  KEY `cr_tasks_level_idx` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_tasks`
--

LOCK TABLES `cr_tasks` WRITE;
/*!40000 ALTER TABLE `cr_tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_templatecachecriteria`
--

DROP TABLE IF EXISTS `cr_templatecachecriteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_templatecachecriteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `criteria` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cr_templatecachecriteria_cacheId_fk` (`cacheId`),
  KEY `cr_templatecachecriteria_type_idx` (`type`),
  CONSTRAINT `cr_templatecachecriteria_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `cr_templatecaches` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_templatecachecriteria`
--

LOCK TABLES `cr_templatecachecriteria` WRITE;
/*!40000 ALTER TABLE `cr_templatecachecriteria` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_templatecachecriteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_templatecacheelements`
--

DROP TABLE IF EXISTS `cr_templatecacheelements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  KEY `cr_templatecacheelements_cacheId_fk` (`cacheId`),
  KEY `cr_templatecacheelements_elementId_fk` (`elementId`),
  CONSTRAINT `cr_templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `cr_templatecaches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_templatecacheelements`
--

LOCK TABLES `cr_templatecacheelements` WRITE;
/*!40000 ALTER TABLE `cr_templatecacheelements` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_templatecacheelements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_templatecaches`
--

DROP TABLE IF EXISTS `cr_templatecaches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_templatecaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cr_templatecaches_expiryDate_cacheKey_locale_path_idx` (`expiryDate`,`cacheKey`,`locale`,`path`),
  KEY `cr_templatecaches_locale_fk` (`locale`),
  CONSTRAINT `cr_templatecaches_locale_fk` FOREIGN KEY (`locale`) REFERENCES `cr_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_templatecaches`
--

LOCK TABLES `cr_templatecaches` WRITE;
/*!40000 ALTER TABLE `cr_templatecaches` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_templatecaches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_tokens`
--

DROP TABLE IF EXISTS `cr_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `route` text COLLATE utf8_unicode_ci,
  `usageLimit` tinyint(3) unsigned DEFAULT NULL,
  `usageCount` tinyint(3) unsigned DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_tokens_token_unq_idx` (`token`),
  KEY `cr_tokens_expiryDate_idx` (`expiryDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_tokens`
--

LOCK TABLES `cr_tokens` WRITE;
/*!40000 ALTER TABLE `cr_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_usergroups`
--

DROP TABLE IF EXISTS `cr_usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_usergroups_name_unq_idx` (`name`),
  UNIQUE KEY `cr_usergroups_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_usergroups`
--

LOCK TABLES `cr_usergroups` WRITE;
/*!40000 ALTER TABLE `cr_usergroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_usergroups_users`
--

DROP TABLE IF EXISTS `cr_usergroups_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_usergroups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  KEY `cr_usergroups_users_userId_fk` (`userId`),
  CONSTRAINT `cr_usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `cr_usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `cr_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_usergroups_users`
--

LOCK TABLES `cr_usergroups_users` WRITE;
/*!40000 ALTER TABLE `cr_usergroups_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_usergroups_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_userpermissions`
--

DROP TABLE IF EXISTS `cr_userpermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_userpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_userpermissions_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_userpermissions`
--

LOCK TABLES `cr_userpermissions` WRITE;
/*!40000 ALTER TABLE `cr_userpermissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_userpermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_userpermissions_usergroups`
--

DROP TABLE IF EXISTS `cr_userpermissions_usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_userpermissions_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  KEY `cr_userpermissions_usergroups_groupId_fk` (`groupId`),
  CONSTRAINT `cr_userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `cr_usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `cr_userpermissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_userpermissions_usergroups`
--

LOCK TABLES `cr_userpermissions_usergroups` WRITE;
/*!40000 ALTER TABLE `cr_userpermissions_usergroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_userpermissions_usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_userpermissions_users`
--

DROP TABLE IF EXISTS `cr_userpermissions_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_userpermissions_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  KEY `cr_userpermissions_users_userId_fk` (`userId`),
  CONSTRAINT `cr_userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `cr_userpermissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `cr_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_userpermissions_users`
--

LOCK TABLES `cr_userpermissions_users` WRITE;
/*!40000 ALTER TABLE `cr_userpermissions_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `cr_userpermissions_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_users`
--

DROP TABLE IF EXISTS `cr_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preferredLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weekStartDay` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `client` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `suspended` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pending` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIPAddress` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(4) unsigned DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `verificationCode` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwordResetRequired` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cr_users_username_unq_idx` (`username`),
  UNIQUE KEY `cr_users_email_unq_idx` (`email`),
  KEY `cr_users_verificationCode_idx` (`verificationCode`),
  KEY `cr_users_uid_idx` (`uid`),
  KEY `cr_users_preferredLocale_fk` (`preferredLocale`),
  CONSTRAINT `cr_users_id_fk` FOREIGN KEY (`id`) REFERENCES `cr_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cr_users_preferredLocale_fk` FOREIGN KEY (`preferredLocale`) REFERENCES `cr_locales` (`locale`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_users`
--

LOCK TABLES `cr_users` WRITE;
/*!40000 ALTER TABLE `cr_users` DISABLE KEYS */;
INSERT INTO `cr_users` VALUES (1,'admin',NULL,NULL,NULL,'chuyen.luu@ffwagency.com','$2y$13$.5LjPWImAPUg2UvPkQwPSOT6h7yy.YN1WPvs9Kb8NsKE9Ehwfm3iy',NULL,0,1,0,0,0,0,0,'2017-11-20 02:38:07','192.168.64.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2017-11-15 02:22:09','2017-11-15 02:22:09','2017-11-20 02:38:07','a75e8310-9552-460a-86ed-e3323755a424');
/*!40000 ALTER TABLE `cr_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cr_widgets`
--

DROP TABLE IF EXISTS `cr_widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cr_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `colspan` tinyint(4) unsigned DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cr_widgets_userId_fk` (`userId`),
  CONSTRAINT `cr_widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `cr_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cr_widgets`
--

LOCK TABLES `cr_widgets` WRITE;
/*!40000 ALTER TABLE `cr_widgets` DISABLE KEYS */;
INSERT INTO `cr_widgets` VALUES (1,1,'RecentEntries',1,NULL,NULL,1,'2017-11-15 02:22:14','2017-11-15 02:22:14','89503053-e9b8-459d-a5df-df5dba939840'),(2,1,'GetHelp',2,NULL,NULL,0,'2017-11-15 02:22:14','2017-11-15 02:22:26','f6af3a21-1156-4e1e-8d9e-eff8b589ffc9'),(3,1,'Updates',3,NULL,NULL,1,'2017-11-15 02:22:14','2017-11-15 02:22:14','12b5be29-0679-46e7-bc99-e6ac99f2b556'),(4,1,'Feed',4,NULL,'{\"url\":\"https:\\/\\/craftcms.com\\/news.rss\",\"title\":\"Craft News\"}',0,'2017-11-15 02:22:14','2017-11-15 02:22:30','bdef31e2-8966-4741-a764-7971e04a1fc4');
/*!40000 ALTER TABLE `cr_widgets` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-20  4:20:29
